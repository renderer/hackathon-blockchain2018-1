#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface NearbyManager : RCTEventEmitter <RCTBridgeModule>
@end
