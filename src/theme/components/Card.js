import palette from '../palette';

export default (variables, defaultTheme) => {
  return {
    '.bgMain': {
      backgroundColor: palette.primary.main,
      borderColor: palette.primary.main,
      'NativeBase.CardItem': {
        backgroundColor: palette.primary.main
      }
    },
    '.bgSemiDark': {
      backgroundColor: palette.primary.semiDark,
      borderColor: palette.primary.semiDark,
      'NativeBase.CardItem': {
        backgroundColor: palette.primary.semiDark
      }
    },
    '.bgDark': {
      backgroundColor: palette.primary.dark,
      borderColor: palette.primary.dark,
      'NativeBase.CardItem': {
        backgroundColor: palette.primary.dark
      }
    },
    '.first': {
      marginTop: palette.spacingUnit * 4
    },
    '.transparent': {
      ...defaultTheme['.transparent'],
      backgroundColor: 'transparent',
      borderWidth: 0,
      'NativeBase.CardItem': {
        backgroundColor: 'transparent'
      }
    },
    '.noHeader': {
      'NativeBase.CardItem': {
        paddingTop: palette.spacingUnit * 4,
        paddingBottom: palette.spacingUnit * 4
      }
    },
    '.smallMargin': {
      '.first': {
        marginTop: palette.spacingUnit * 2
      },
      marginBottom: palette.spacingUnit * 2
    },
    '.smallestMargin': {
      marginBottom: palette.spacingUnit
    },
    marginTop: 0,
    marginBottom: palette.spacingUnit * 4
  };
};
