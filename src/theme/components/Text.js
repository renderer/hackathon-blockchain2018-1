import palette from '../palette';

export default variables => {
  return {
    '.error': {
      color: palette.text.error
    },
    '.center': {
      textAlign: 'center',
      alignItems: 'center'
    },
    '.bold': {
      fontWeight: 'bold'
    },
    '.blur': {
      color: palette.text.blur
    },
    '.highlight': {
      color: palette.text.highlight
    },
    '.italic': {
      fontStyle: 'italic'
    },
    '.important': {
      color: palette.text.important
    },
    '.inputPlaceholder': {
      paddingLeft: variables.spacingUnit,
      fontSize: variables.inputFontSize,
      color: palette.text.placeholder
    },
    '.inputValue': {
      paddingLeft: variables.spacingUnit,
      fontSize: variables.inputFontSize
    },
    '.primary': {
      color: palette.primary.main
    },
    '.contrast': {
      color: palette.text.contrast
    },
    '.right': {
      textAlign: 'right',
      marginRight: variables.spacingUnit * 4
    },
    '.padVertical': {
      marginLeft: variables.spacingUnit * 4,
      marginRight: variables.spacingUnit * 4
    },

    '.endDescription': {
      marginBottom: variables.spacingUnit * 6
    }
  };
};
