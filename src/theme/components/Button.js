export default (variables, defaultTheme) => {
  return {
    'NativeBase.Text': {
      fontSize: variables.fontSizeH3,
      fontWeight: 'bold'
    },
    '.small': {
      ...defaultTheme['.small'],
      height: variables.spacingUnit * 8,
      'NativeBase.Text': {
        fontSize: variables.fontSizeBase * 1.5,
        fontWeight: 'bold'
      }
    },
    '.colMargin': {
      marginTop: variables.spacingUnit * 2,
      marginBottom: variables.spacingUnit * 2
    },
    '.fixedWidth': {
      width: variables.spacingUnit * 24,
      justifyContent: 'center',
      'NativeBase.Text': {
        fontSize: variables.fontSizeBase * 1.2,
        fontWeight: 'bold'
      }
    }
  };
};
