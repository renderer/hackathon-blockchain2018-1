import palette from '../palette';

export default variables => {
  return {
    '.authScreenWrapper': {
      width: '80%'
    },
    '.bgMain': {
      backgroundColor: palette.primary.main
    },
    '.bgBlur': {
      backgroundColor: palette.primary.blur
    },
    '.bgDikeSpecial': {
      backgroundColor: palette.dikeNumber.special
    },
    '.bgDikeNormal': {
      backgroundColor: palette.dikeNumber.normal
    },
    '.bgSemiDark': {
      backgroundColor: palette.primary.semiDark
    },
    '.borderBottom': {
      borderBottomWidth: 1,
      borderBottomColor: palette.text.blur,
      marginBottom: variables.spacingUnit * 2
    },
    '.borderRight': {
      borderRightWidth: 1,
      borderRightColor: palette.text.blur,
      marginRight: variables.spacingUnit * 2
    },
    '.btnBg': {
      width: variables.spacingUnit * 4,
      height: variables.spacingUnit * 4,
      borderRadius: variables.spacingUnit * 2
    },
    '.center': {
      justifyContent: 'center',
      alignItems: 'center'
    },
    '.flexItem': {
      flex: 1
    },
    '.full': {
      flexGrow: 1,
      width: '100%',
      height: '100%'
    },
    '.fullWidth': {
      width: '100%'
    },
    '.fullFormFooter': {
      alignItems: 'center',
      'NativeBase.Button': {
        marginBottom: variables.spacingUnit * 3
      }
    },
    '.half': {
      width: '50%'
    },
    '.horizontal': {
      flexDirection: 'row'
    },
    '.paddingRow': {
      paddingLeft: variables.spacingUnit * 4,
      paddingRight: variables.spacingUnit * 4,
      paddingTop: variables.spacingUnit,
      paddingBottom: variables.spacingUnit
    },
    '.input': {
      paddingRight: variables.spacingUnit * 4,
      paddingLeft: variables.spacingUnit * 4,
      paddingTop: variables.spacingUnit * 2,
      paddingBottom: variables.spacingUnit * 2,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
      backgroundColor: palette.text.main,
      borderRadius: 4
    },
    '.marginAround': {
      margin: variables.spacingUnit * 4
    },
    '.overlay': {
      backgroundColor: 'rgba(0, 0, 0, 0.6)',
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      elevation: 25,
      zIndex: 25
    },
    '.paddingContent': {
      padding: variables.spacingUnit * 10
    },
    '.padVertical': {
      paddingTop: variables.spacingUnit * 10,
      paddingBottom: variables.spacingUnit * 10
    },
    '.revert': {
      justifyContent: 'flex-end'
    },
    '.rowFlexItem': {
      flex: 1,
      marginLeft: variables.spacingUnit,
      marginRight: variables.spacingUnit
    },
    '.round': {
      borderRadius: 2
    },
    '.textBetween': {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'baseline'
    },
    '.textAround': {
      justifyContent: 'space-around', 
      flexDirection: 'row'
    },

    '.wrap': {
      flexWrap: 'wrap'
    },
    '.wrapContainer': {
      marginBottom: -(variables.spacingUnit * 2)
    },
    '.wrapItem': {
      marginBottom: variables.spacingUnit * 2
    }
  };
};
