import palette from '../palette';

export default variables => {
  return { fontSize: variables.fontSizeH3, color: palette.text.main };
};
