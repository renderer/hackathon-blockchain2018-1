import palette from '../palette';

export default variables => ({
  '.bold': {
    fontWeight: 'bold'
  },
  '.viewTitle': {
    marginBottom: palette.spacingUnit * 4
  },
  '.center': {
    textAlign: 'center'
  },
  '.title': {
    marginBottom: palette.spacingUnit * 4
  }
});
