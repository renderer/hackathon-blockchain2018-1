import palette from '../palette';

export default variables => ({
  '.bold': {
    fontWeight: 'bold'
  },
  '.important': {
    color: palette.text.important
  },
  '.highlight': {
    color: palette.text.highlight
  },
  '.center': {
    textAlign: 'center'
  },
  '.blur': {
    color: palette.text.blur
  },

  '.title': {
    marginBottom: palette.spacingUnit * 4
  }
});
