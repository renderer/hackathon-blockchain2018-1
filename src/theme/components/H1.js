import palette from '../palette';

export default variables => ({
  '.bold': {
    fontWeight: 'bold'
  },
  '.large': {
    fontSize: variables.fontSizeBase * 2
  },
  '.larger': {
    fontSize: variables.fontSizeBase * 2.4,
    lineHeight: variables.fontSizeBase * 2.4 + 2
  },
  '.contrast': {
    color: palette.text.contrast
  }
});
