import palette from '../palette';

export default variables => {
  return {
    '.borderBottom': {
      borderBottomWidth: 1,
      borderBottomColor: palette.primary.blur,
      marginLeft: variables.spacingUnit * 4,
      marginRight: variables.spacingUnit * 4,
      'NativeBase.Left': {
        marginLeft: -(variables.spacingUnit * 4)
      },
      'NativeBase.Right': {
        marginRight: -(variables.spacingUnit * 4)
      },
      'NativeBase.Body': {
        marginLeft: -(variables.spacingUnit * 4),
        marginRight: -(variables.spacingUnit * 4)
      }
    },
    '.endFlexBody': {
      marginTop: -(variables.spacingUnit * 2),
      justifyContent: 'space-between'
    },
    '.center': {
      justifyContent: 'center',
      alignItems: 'center'
    },
    '.vertical': {
      flexDirection: 'column'
    },
    '.fullWidth': {
      paddingLeft: -(variables.spacingUnit * 4),
      paddingRight: -(variables.spacingUnit * 4)
    },
    '.smallPadding': {
      paddingTop: variables.spacingUnit * 2,
      paddingBottom: variables.spacingUnit * 2
    },
    padding: variables.spacingUnit * 4
  };
};
