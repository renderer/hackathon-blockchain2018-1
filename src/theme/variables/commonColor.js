import palette from '../palette';

export default {
  // Button
  btnPrimaryBg: palette.button.primary,
  btnInfoBg: palette.button.info,
  btnSuccessBg: palette.dikeNumber.normal,
  btnLineHeight: 31,
  btnDisabledBg: palette.primary.blur,

  // Font
  fontSizeBase: palette.fontSizeBase,

  // List
  listBorderColor: palette.text.blur,

  // Text
  textColor: palette.text.main,

  // InputGroup
  inputColor: palette.text.main,
  inputColorPlaceholder: palette.text.placeholder,
  inputHeightBase: palette.spacingUnit * 10,
  inputFontSize: palette.inputFontSize,
  inputBorderColor: palette.inputBorderColor,
  inputErrorBorderColor: palette.inputErrorBorderColor,

  // Spinner
  defaultSpinnerColor: palette.text.main,

  // Other
  spacingUnit: palette.spacingUnit
};
