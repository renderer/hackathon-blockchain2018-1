import { AsyncStorage, Platform } from 'react-native';

let setStorageItem;
let getStorageItem;
let removeStorageItem;

if (global.__DEV__ === true && Platform.OS === 'android') {
  const userToken = null;
  // const userToken =
  //   'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViNmFjNjQxMmE3N2FhNWJjZGI4MzdiOSIsInBhc3N3b3JkIjoiZDZiMGFiN2YxYzhhYjhmNTE0ZGI5YTZkODVkZTE2MGEiLCJpYXQiOjE1MzY1MTM4NDZ9.5lOdVjX3jN5CqmleYSkxewC3n-Ov2F1FIEkBkmEiLAA';
  setStorageItem = async (key, value) => null;
  getStorageItem = async key => {
    if (key === 'userToken') {
      return userToken;
    }
    return null;
  };
  removeStorageItem = async key => null;
} else {
  setStorageItem = async (key, value) => {
    await AsyncStorage.setItem(key, value);
  };
  getStorageItem = async key => {
    const value = await AsyncStorage.getItem(key);
    return value;
  };
  removeStorageItem = async key => {
    await AsyncStorage.removeItem(key);
  };
}

export { setStorageItem, getStorageItem, removeStorageItem };
