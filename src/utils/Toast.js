import { Toast } from 'native-base';

export default {
  show(text, options = {}) {
    try {
      if (Toast) {
        Toast.show({
          text,
          buttonText: 'OK',
          position: 'bottom',
          type: 'success',
          textStyle: { fontSize: 20 },
          duration: 2000,
          ...options
        });
      }
    } catch (e) {
      return;
    }
  }
};
