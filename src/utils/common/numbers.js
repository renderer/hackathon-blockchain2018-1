export const padZero = (number, pad = 2) => {
  let str = `${number}`;
  while (str.length < pad) {
    str = `0${str}`;
  }
  return str;
};

export const isEven = n => n % 2 === 0;
