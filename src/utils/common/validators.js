/* eslint-disable */
const emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const usernameReg = /[a-zA-Z0-9]+/;

export const validateEmail = email => email && emailReg.test(email.toLowerCase());
export const validateUsername = username =>
  username && username.length >= 3 && username.length <= 20 && usernameReg.test(username);
export const validatePassword = password =>
  password && password.length >= 7 && password.length <= 100;
