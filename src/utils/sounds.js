import Sound from 'react-native-sound';

Sound.setCategory('Playback');

const createSound = srcFileName => {
  const sound = new Sound(srcFileName, Sound.MAIN_BUNDLE);
  sound.setNumberOfLoops(0);
  return sound;
};

export const blip = createSound('blip.wav');
export const chip = createSound('chip.mp3');
export const tap = createSound('tap.wav');
export const whoosh = createSound('whoosh.wav');
