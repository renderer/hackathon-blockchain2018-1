import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';

import config from 'dike/config';

let token;
export const setToken = newToken => (token = newToken);

const createGraphClient = uri => {
  const httpLink = createHttpLink({ uri });
  const authLink = setContext((_, { headers }) => {
    const newHeaders = { ...headers };
    if (token) {
      newHeaders['x-access-token'] = token;
    }
    return {
      headers: newHeaders
    };
  });

  const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
    defaultOptions: { query: { fetchPolicy: 'network-only' } }
  });

  return client;
};
export const drawingGraphClient = createGraphClient(config.drawingGraphUrl);
export const userGraphClient = createGraphClient(config.userGraphUrl);
