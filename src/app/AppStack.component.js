import React from 'react';
import { TabNavigator } from 'react-navigation';
import { Platform } from 'react-native';

import { EmailVerifyScreen } from 'dike/screen/emailVerify';

import UserStack from 'dike/stack/UserStack';
import AuthStack from 'dike/stack/AuthStack';
import ShopStack from 'dike/stack/ShopStack';

import DikeBottomBar from 'dike/components/DikeBottomBar';
import DikeResultPopup from 'dike/components/DikeResultPopup';

const AppStack = TabNavigator(
  {
    User: { screen: UserStack },
    Shop: { screen: ShopStack }
  },
  {
    initialRouteName: 'User',
    tabBarComponent: DikeBottomBar,
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: false
  }
);
const prefix = Platform.OS === 'android' ? 'dikeapp://dikeapp/' : 'dikeapp://';

export default ({ isLoggedIn, emailVerified }) => (
  <React.Fragment>
    {isLoggedIn && !emailVerified && <EmailVerifyScreen />}
    {!isLoggedIn && <AuthStack uriPrefix={prefix} />}
    {isLoggedIn &&
      emailVerified && (
        <React.Fragment>
          <AppStack />
          <DikeResultPopup />
        </React.Fragment>
      )}
  </React.Fragment>
);
