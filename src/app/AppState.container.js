import { connect } from 'react-redux';
import AppState from './AppState.component';

import { refreshApp } from 'dike/store/app/app.actions';
import { checkToken } from 'dike/store/user/user.actions';

export default connect(
  null,
  dispatch => ({
    onRefreshApp() {
      dispatch(refreshApp());
    },
    onCheckToken() {
      dispatch(checkToken());
    }
  })
)(AppState);
