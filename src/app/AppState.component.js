import React, { Component } from 'react';
import { AppState, NetInfo } from 'react-native';
import AppStack from './AppStack.container';

export default class DikeAppState extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
      isConnected: true
    };
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
    this.handleNetInfoChange = this.handleNetInfoChange.bind(this);
  }

  componentDidMount() {
    this.props.onCheckToken();
    AppState.addEventListener('change', this.handleAppStateChange);
    NetInfo.addEventListener('connectionChange', this.handleNetInfoChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
    NetInfo.removeEventListener('connectionChange', this.handleNetInfoChange);
  }

  handleNetInfoChange() {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (!this.state.isConnected && isConnected) {
        this.props.onRefreshApp();
      }
      this.setState({ isConnected: isConnected });
    });
  }

  handleAppStateChange(nextAppState) {
    if (
      this.state &&
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      this.props.onRefreshApp();
    }
    this.setState({ appState: nextAppState });
  }

  render() {
    return <AppStack />;
  }
}
