import dikeIconTheme from 'dike/theme/components/Icon';
import variable from './../variables/platform';

export default (variables = variable) => {
  const iconTheme = {
    fontSize: variables.iconFontSize,
    color: '#000',
    ...dikeIconTheme(variables)
  };

  return iconTheme;
};
