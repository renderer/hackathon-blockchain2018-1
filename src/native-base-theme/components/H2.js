import dikeH2Theme from 'dike/theme/components/H2';
import variable from './../variables/platform';

export default (variables = variable) => {
  const h2Theme = {
    color: variables.textColor,
    fontSize: variables.fontSizeH2,
    lineHeight: variables.lineHeightH2
  };

  return { ...h2Theme, ...dikeH2Theme(variables) };
};
