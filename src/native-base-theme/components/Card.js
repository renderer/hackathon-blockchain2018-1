import dikeCardTheme from 'dike/theme/components/Card';
import variable from './../variables/platform';

export default (variables = variable) => {
  const cardTheme = {
    '.transparent': {
      shadowColor: null,
      shadowOffset: null,
      shadowOpacity: null,
      shadowRadius: null,
      elevation: null
    },
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 2,
    marginRight: 2,
    // marginVertical: 5,
    // marginHorizontal: 2,
    flex: 1,
    borderWidth: variables.borderWidth,
    borderRadius: 2,
    borderColor: variables.cardBorderColor,
    flexWrap: 'nowrap',
    backgroundColor: variables.cardDefaultBg,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 1.5,
    elevation: 3
  };

  return { ...cardTheme, ...dikeCardTheme(variables, cardTheme) };
};
