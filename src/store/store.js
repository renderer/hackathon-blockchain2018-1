import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import socketMiddleware from './socketMiddleware';

import app from './app/app.reducer';
import user from './user/user.reducer';
import drawing from './drawing/drawing.reducer';
import nearby from './nearby/nearby.reducer';

import { fetchNearby } from './nearby/nearby.actions';

import * as NearbyManager from '../../NearbyManager';

const rootReducer = combineReducers({
  app,
  user,
  drawing,
  nearby
});

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(
      thunk.withExtraArgument({
        socketMiddleware
      })
    )
  )
);

NearbyManager.init();
NearbyManager.startDiscovering();

NearbyManager.onUserChanged(users => {
  store.dispatch(fetchNearby(users));
});

export default store;
