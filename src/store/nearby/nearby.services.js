import gql from 'graphql-tag';
import { userGraphClient as client } from 'dike/utils/graphClients';

export default {
  fetchAllNearby: async ({ users }) => {
    const ids = [];
    const proximities = {};

    for (const user of users) {
      ids.push(user.name);
      proximities[user.name] = user.proximity;
    }

    const query = gql`
    query{
      nearby(ids: ${JSON.stringify(ids)}){
        _id
        username
        email
        profilePicture
        displayName
        ethDepositAddress
        activated
        activeCode
        emailVerified
        allowMember
        allowService
      }
    }`;
    const result = await client.query({ query });

    return result.data.nearby.map(user => ({
      ...user,
      proximity: proximities[user._id]
    }));
  }
};
