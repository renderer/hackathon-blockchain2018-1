import * as types from './app.types';
import { SIGN_OUT } from 'dike/store/user/user.types';

const initialState = {
  isAppRefreshing: false,
  isDrawingDetailLoading: true,
  isDrawingHistoryScreenLoading: true,
  isLastBetLoading: true,
  isLoginScreenLoading: false,
  isRegisterScreenLoading: false,
  isPlaceBetsScreenLoading: false,
  isPreviousDrawingLoading: true,
  isTransactionsScreenLoading: false,
  isWalletScreenLoading: false,
  isVerifyEmailScreenLoading: false,
  isReportIssueScreenLoading: false,
  isPeopleScreenLoading: true,
  placeBetsPopup: {
    isVisible: false,
    title: '',
    notice: ''
  },
  walletPopup: {
    isVisible: false,
    title: '',
    notice: ''
  },
  forgotPasswordScreen: {
    isLoading: false,
    error: '',
    errorSetPassword: ''
  },
  changePasswordScreen: {
    isLoading: false,
    errorSubmit: ''
  },
  dikeResultPopup: {
    activeType: 'Raw',
    result: [],
    visible: false
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.SET_APP_REFRESHING:
      return { ...state, isAppRefreshing: action.payload };
    case types.SET_DRAWING_DETAIL_LOADING:
      return { ...state, isDrawingDetailLoading: action.payload };
    case types.SET_DRAWING_HISTORY_LOADING:
      return { ...state, isDrawingHistoryScreenLoading: action.payload };
    case types.SET_LAST_BET_LOADING:
      return { ...state, isLastBetLoading: action.payload };
    case types.SET_LOGIN_SCREEN_LOADING:
      return { ...state, isLoginScreenLoading: action.payload };
    case types.SET_REGISTER_SCREEN_LOADING:
      return { ...state, isRegisterScreenLoading: action.payload };
    case types.SET_PLACE_BETS_SCREEN_LOADING:
      return { ...state, isPlaceBetsScreenLoading: action.payload };
    case types.SET_PREVIOUS_DRAWING_LOADING:
      return { ...state, isPreviousDrawingLoading: action.payload };
    case types.SET_TRANSACTION_HISTORY_SCREEN_LOADING:
      return { ...state, isTransactionsScreenLoading: action.payload };
    case types.SET_WALLET_SCREEN_LOADING:
      return { ...state, isWalletScreenLoading: action.payload };
    case types.SET_PLACE_BETS_POPUP_PROPS:
      return { ...state, placeBetsPopup: { ...state.placeBetsPopup, ...action.payload } };
    case types.SET_VERIFY_EMAIL_SCREEN_LOADING:
      return { ...state, isVerifyEmailScreenLoading: action.payload };
    case types.SET_WALLET_POPUP_PROPS:
      return { ...state, walletPopup: { ...state.walletPopup, ...action.payload } };
    case types.SET_FORGOT_PASSWORD_SCREEN_PROPS:
      return {
        ...state,
        forgotPasswordScreen: { ...state.forgotPasswordScreen, ...action.payload }
      };
    case types.SET_CHANGE_PASSWORD_SCREEN_PROPS:
      return {
        ...state,
        changePasswordScreen: { ...state.changePasswordScreen, ...action.payload }
      };
    case types.SET_REPORT_ISSUE_SCREEN_LOADING:
      return { ...state, isReportIssueScreenLoading: action.payload };
    case types.SET_DIKE_RESULT_POPUP_PROPS:
      return { ...state, dikeResultPopup: { ...state.dikeResultPopup, ...action.payload } };
    case SIGN_OUT:
      return { ...initialState };
    case types.SET_PEOPLE_SCREEN_LOADING:
      return { ...initialState, isPeopleScreenLoading: action.payload };
    default:
      return state;
  }
};
