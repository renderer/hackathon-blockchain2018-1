import moment from 'moment';
import * as types from './user.types';

const initialState = {
  _id: '',
  username: '',
  email: '',
  displayName: '',
  profilePicture: null,
  ethDepositAddress: '',
  ethPrivateKey: '',
  emailVerified: false,
  activated: false,
  balance: 0,
  registerError: null,
  loginError: null,
  hasVerifyEmailError: false,
  allowMember: false,
  allowService: false,
  transactions: null,
  transactionHistory: {
    fromDate: moment().add(-1, 'days'),
    toDate: moment(),
    transactions: null
  },
  rankingData: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case `${types.LOGIN}_FAIL`: {
      try {
        return {
          ...state,
          loginError: action.error.graphQLErrors[0].message
        };
      } catch (e) {
        return state;
      }
    }
    case `${types.REGISTER}_FAIL`: {
      try {
        const errors = JSON.parse(action.error.graphQLErrors[0].message);
        return { ...state, registerError: { ...errors } };
      } catch (e) {
        return state;
      }
    }

    case types.CLEAR_REGISTER_ERROR:
      return { ...state, registerError: null };
    case types.CLEAR_LOGIN_ERROR:
      return { ...state, loginError: null };
    case types.SET_VERIFY_EMAIL_ERROR:
      return { ...state, hasVerifyEmailError: action.payload };
    case types.EMAIL_VERIFIED:
      return { ...state, emailVerified: true };

    case `${types.ME}_SUCCESS`:
      return { ...state, ...action.payload };

    case `${types.GET_BALANCE}_SUCCESS`:
      return { ...state, balance: action.payload };

    case `${types.GET_TRANSACTION_HISTORY}_SUCCESS`:
      return {
        ...state,
        transactionHistory: { ...state.transactionHistory, transactions: action.payload }
      };
    case types.CHANGE_TRANSACTION_HISTORY_DATE:
      return {
        ...state,
        transactionHistory: { ...state.transactionHistory, ...action.payload }
      };
    case `${types.GET_LAST_TRANSACTIONS}_SUCCESS`:
      return { ...state, transactions: action.payload };

    case `${types.GET_RANKING}_SUCCESS`:
      return { ...state, rankingData: action.payload };

    case types.SIGN_OUT:
      return { ...initialState };
    default:
      return state;
  }
};
