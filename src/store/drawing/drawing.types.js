export const GET_LAST_BET = 'drawing/GET_LAST_BET';
export const GET_DRAWING_DETAIL = 'drawing/GET_DRAWING_DETAIL';
export const GET_DRAWING_HISTORY = 'drawing/GET_DRAWING_HISTORY';
export const GET_DRAWING_STATUS = 'drawing/GET_DRAWING_STATUS';
export const PLACE_BETS = 'drawing/PLACE_BETS';
export const GET_PREVIOUS_DRAWING = 'drawing/GET_PREVIOUS_DRAWING';

export const SET_DRAWING_HISTORY_PROPS = 'drawing/SET_DRAWING_HISTORY_PROPS';
export const REMOVE_CURRENT_DRAWING = 'drawing/REMOVE_CURRENT_DRAWING';
export const SET_CURRENT_DRAWING_STATS = 'drawing/SET_CURRENT_DRAWING_STATS';
export const SET_USER_BETS = 'drawing/SET_USER_BETS';
export const GET_DRAWING_CHART = 'drawing/GET_DRAWING_CHART';
