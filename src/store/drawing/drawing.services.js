import gql from 'graphql-tag';
import { drawingGraphClient as client } from 'dike/utils/graphClients';

export default {
  getLastBet: async () => {
    const query = gql`
      query {
        lastParticipatedDrawing {
          id
          endTime
          personalStats {
            totalReward
            rewardDetails {
              number
              stake
              reward
              special
              occurence
            }
          }
        }
      }
    `;

    const result = await client.query({ query });
    const data = result.data.lastParticipatedDrawing;
    if (!data) {
      return {};
    }

    return {
      drawingId: data.id,
      bets: data.personalStats.rewardDetails,
      drawingDate: data.endTime,
      totalReward: data.personalStats.totalReward
    };
  },

  getDrawingDetail: async id => {
    const query = gql`
      query {
        singleDrawing(id: "${id}") {
          id
          startTime
          endTime
          done
          result
          serial
          data
          drawingStats {
            betCount
            globalStake
            stakeLeaderboard {
              username
              stake
            }
            rewardLeaderboard {
              username
              reward
            }
          }
          userBets {
            number
            stake
            reward
            special
          }
          participated
          personalStats {
            totalStake
            totalReward
            rewardDetails {
              number
              stake
              reward
              special
              occurence
            }
          }
        }
      }
    `;
    const result = await client.query({ query });
    const data = result.data.singleDrawing;
    return { ...data };
  },

  getDrawingHistory: async ({ date, participated }) => {
    const query = gql`
      query {
        drawingHistory (date: ${date}, participated: ${participated}) {
          id
          endTime
          done
          result
          serial
          participated
        }
      }
    `;

    const result = await client.query({ query });
    const data = result.data.drawingHistory;

    return data;
  },

  getDrawingStatus: async () => {
    const query = gql`
      query {
        drawingStatus {
          inProgress
          currentDrawing {
            id
            startTime
            endTime
            done
            participated
            userBets {
              number
              stake
              special
            }
            drawingStats {
              betCount
              globalStake
              stakeLeaderboard {
                username
                stake
              }
            }
          }
          nextAt
        }
      }
    `;

    const result = await client.query({ query });
    const data = result.data.drawingStatus;

    return data;
  },

  placeBets: async bets => {
    bets = bets.map(bet => {
      bet.numbers = bet.numbers.map(numb => parseInt(numb, 10));
      return bet;
    });
    //TODO: find a way to pass array
    const mutate = gql`
      mutation {
        placeBets(bets: "${JSON.stringify(bets).replace(/"/g, "'")}")
      }
    `;

    const result = await client.mutate({
      mutation: mutate
    });

    return result.data.placeBets;
  },

  getPreviousDrawing: async () => {
    const query = gql`
      query {
        previousDrawing {
          id
          startTime
          endTime
          done
          result
          data
          drawingStats {
            rewardLeaderboard {
              username
              reward
            }
          }
        }
      }
    `;

    const result = await client.query({ query });
    return result.data.previousDrawing;
  },

  getDrawingChart: async () => {
    const query = gql`
      query {
        drawingChart {
          lastSpecials
          topHitNumbers
          topMissNumbers
        }
      }
    `;
    const result = await client.query({ query });
    return result.data.drawingChart;
  }
};
