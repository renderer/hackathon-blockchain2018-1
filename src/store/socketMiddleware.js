import io from 'socket.io-client';
import config from 'dike/config';

import Toast from 'dike/utils/Toast';

import { getBalance, getLastTransactions } from './user/user.actions';

class SocketMiddleware {
  constructor() {
    this.socket = io(config.socketBaseUrl, { autoConnect: false });
  }

  createInstance() {
    this.socket = io(config.socketBaseUrl, { autoConnect: false });
    return this.socket;
  }

  getInstance() {
    return this.socket;
  }

  addListeners(dispatch, getState) {
    const socket = this.getInstance();
    socket.connect();

    const { user } = getState();
    if (!user.activated) {
      socket.emit('room', getState().user._id);
      socket.on('userActivated', () => {
        dispatch(getBalance('dike'));
        Toast.show('Your account has been activated');
      });
    }

    
  }

  reconnect(dispatch, getState) {
    this.createInstance();
    this.addListeners(dispatch, getState);

    dispatch(getBalance('dike'));
    dispatch(getLastTransactions());
  }

  disconnect() {
    const socket = this.getInstance();
    socket.disconnect();
  }
}

const socketMiddleware = new SocketMiddleware();

export default socketMiddleware;
