import { tap } from 'dike/utils/sounds';
import theme from 'dike/theme';
import React from 'react';

import { TouchableOpacity, StyleSheet } from 'react-native';
import { View, Text, Icon } from 'native-base';

const styles = StyleSheet.create({
  keyBoard: { marginLeft: 2, marginRight: 2 },
  pinKeyContainer: { width: '33%' },
  pinKeyBox: { padding: 1 },
  pinKeyIcon: { color: theme.text.blur }
});

const PinKey = ({ label, onPress, height }) => {
  const fontSize = height / 9;
  const pinContainerHeight = (height - 4) / 4;
  return (
    <TouchableOpacity
      onPressIn={() => {
        if (!!label && onPress) {
          onPress(label);
          tap.play();
        }
      }}
      style={[styles.pinKeyContainer, { height: pinContainerHeight }]}>
      <View flexItem center style={styles.pinKeyBox}>
        {!!label && (
          <View
            center
            full
            bgMain={['Special', 'Normal'].some(l => l !== label)}
            round
            bgDikeSpecial={label === 'Special'}
            bgDikeNormal={label === 'Normal'}>
            {label !== 'BackSpace' && (
              <Text blur={['Special', 'Normal'].some(l => l !== label)} style={{ fontSize }}>
                {label}
              </Text>
            )}
            {label === 'BackSpace' && (
              <Icon style={[styles.pinKeyIcon, { fontSize }]} name="ios-backspace" />
            )}
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
};

export default ({ onInput, height, hideModeBtn = true, isSpecial }) => {
  const buttons = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '', '0', 'BackSpace'];
  if (hideModeBtn) {
    buttons[9] = '';
  } else if (isSpecial) {
    buttons[9] = 'Normal';
  } else {
    buttons[9] = 'Special';
  }
  return (
    <View horizontal center wrap style={[styles.keyBoard, { height }]}>
      {buttons.map((value, i) => (
        <PinKey label={value} key={i} onPress={value => onInput(value)} height={height} />
      ))}
    </View>
  );
};
