import React, { Component } from 'react';
import moment from 'moment';
import { tap } from 'dike/utils/sounds';

import { TouchableOpacity } from 'react-native';
import { Text, View, Icon } from 'native-base';
import DateTimePicker from 'react-native-modal-datetime-picker';

export default class DateSelector extends Component {
  static defaultProps = {
    placeholder: 'Select a date'
  };

  constructor(props) {
    super(props);
    const { defaultValue } = this.props;
    this.state = {
      isPickerVisible: false,
      value: defaultValue || null
    };
  }

  handleDatePicked(date) {
    this.setState({
      isPickerVisible: false,
      value: date
    });
    this.props.onChange(date);
  }

  render() {
    const { placeholder } = this.props;
    const { value, isPickerVisible } = this.state;

    return [
      <TouchableOpacity
        onPressIn={() => {
          this.setState({ isPickerVisible: true });
          tap.play();
        }}
        key="btn">
        <View center horizontal>
          <Icon name="ios-calendar-outline" highlight larger />
          <Text highlight> {value ? moment(value).format('MM/DD/YYYY') : placeholder}</Text>
        </View>
      </TouchableOpacity>,
      <DateTimePicker
        key="picker"
        isVisible={isPickerVisible}
        onConfirm={date => this.handleDatePicked(date)}
        onCancel={() => this.setState({ isPickerVisible: false })}
        maximumDate={moment().toDate()}
        date={moment(this.state.value).toDate()}
      />
    ];
  }
}
