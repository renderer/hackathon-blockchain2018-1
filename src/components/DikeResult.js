import { connect } from 'react-redux';
import theme from 'dike/theme';
import React from 'react';
import { setDikeResultPopupProps } from 'dike/store/app/app.actions';

import { Dimensions, StyleSheet, TouchableOpacity } from 'react-native';
import { View } from 'native-base';

import DikeNumber from './DikeNumber';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  wrapper: { width: `${100 / 7}%` }
});

const DikeResult = ({ result, onSetPopupProps }) => (
  <TouchableOpacity onPressIn={() => onSetPopupProps({ visible: true, result })}>
    <View horizontal wrap>
      {result.map((number, i) => (
        <View center wrapItem style={styles.wrapper} key={i}>
          <DikeNumber
            size={Math.floor((width - 4 - theme.spacingUnit * 18) / 7)}
            number={number}
            special={i === 0}
          />
        </View>
      ))}
    </View>
  </TouchableOpacity>
);

export default connect(
  null,
  dispatch => ({
    onSetPopupProps(props) {
      dispatch(setDikeResultPopupProps(props));
    }
  })
)(DikeResult);
