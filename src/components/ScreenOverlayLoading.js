import React from 'react';
import { View, Spinner } from 'native-base';

export default () => (
  <View center overlay>
    <Spinner />
  </View>
);
