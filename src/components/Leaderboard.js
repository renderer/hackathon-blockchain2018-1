import React from 'react';

import BigNumber from 'bignumber.js';
import { View, Text, H2, H3, Icon } from 'native-base';

import { isEven } from 'dike/utils/common/numbers';

const getRankingColor = i => {
  switch (i) {
    case 0:
      return 'gold';
    case 1:
      return 'silver';
    case 2:
      return '#965A38';
    default:
      return 'black';
  }
};

const getTypo = type => {
  switch (type) {
    case 'reward':
      return {
        title: 'Reward Leaderboard',
        userLabel: 'Username',
        pointLabel: 'Reward'
      };
    case 'stake':
      return {
        title: 'Stake Leaderboard',
        userLabel: 'Username',
        pointLabel: 'Stake'
      };
    default:
      return {
        title: 'Leaderboard',
        userLabel: 'Username',
        pointLabel: 'Points'
      };
  }
};

export default ({ leaderboard, type, style }) => {
  const { title, userLabel, pointLabel } = getTypo(type);
  return (
    <View fullWidth>
      <H2 center bold>
        {title}
      </H2>
      <View horizontal textBetween paddingRow>
        <Text small style={{ marginLeft: 26 }}>
          {userLabel}
        </Text>
        <Text small>{pointLabel}</Text>
      </View>
      {leaderboard.map((row, i) => (
        <View bgMain={isEven(i)} bgSemiDark={!isEven(i)} key={i}>
          <View textBetween paddingRow>
            <View horizontal center>
              <Icon name="ios-ribbon" style={{ color: getRankingColor(i) }} />
              <H3> {row.username}</H3>
            </View>
            <View>
              <H3>{new BigNumber(row.points).toFormat(0)}</H3>
            </View>
          </View>
        </View>
      ))}
    </View>
  );
};
