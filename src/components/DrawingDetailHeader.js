import { connect } from 'react-redux';
import React from 'react';

import { Platform } from 'react-native';
import { Text } from 'native-base';

const getDrawingHeader = (serial, isLoading) => {
  if (isLoading) {
    return 'Drawing';
  }
  if (serial) {
    return `Drawing #${serial}`;
  }
  return 'Drawing';
};

const DrawingDetailHeader = ({ serial, isLoading }) => (
  <Text
    style={{
      fontSize: Platform.OS === 'ios' ? 17 : 20,
      fontWeight: Platform.OS === 'ios' ? '700' : '500',
      textAlign: Platform.OS === 'ios' ? 'center' : 'left',
      marginHorizontal: 16
    }}>
    {getDrawingHeader(serial, isLoading)}
  </Text>
);

export default connect(({ app, drawing }) => ({
  serial: drawing.drawingDetail.serial || '',
  isLoading: app.isDrawingDetailLoading
}))(DrawingDetailHeader);
