import React, { Component } from 'react';
import moment from 'moment';
import { tap } from 'dike/utils/sounds';

import { TouchableOpacity } from 'react-native';
import { View, Text, Icon } from 'native-base';

import DateTimePicker from 'react-native-modal-datetime-picker';

export default class DateRangeSelector extends Component {
  static defaultProps = {
    defaultValue: {
      fromDate: moment().add(-1, 'days'),
      toDate: moment()
    },
    onChange: () => {},
    ViewProps: {}
  };

  constructor(props) {
    super(props);
    const { defaultValue } = this.props;
    this.state = {
      picking: '',
      isPickerVisible: false,
      fromDate: defaultValue.fromDate,
      toDate: defaultValue.toDate
    };
  }

  handleDatePicked(date) {
    const { picking, fromDate, toDate } = this.state;
    const { onChange } = this.props;
    if (picking === 'from') {
      this.setState({ fromDate: date, isPickerVisible: false });
      onChange({ fromDate: date, toDate });
    }
    if (picking === 'to') {
      this.setState({ toDate: date, isPickerVisible: false });
      onChange({ fromDate, toDate: date });
    }
  }

  render() {
    const { fromDate, toDate, isPickerVisible, picking } = this.state;
    const { ViewProps } = this.props;

    return (
      <View textBetween {...ViewProps}>
        <TouchableOpacity
          onPressIn={() => {
            this.setState({ picking: 'from', isPickerVisible: true });
            tap.play();
          }}>
          <View horizontal center>
            <Icon name="ios-calendar-outline" highlight larger />
            <Text bold highlight>
              {' '}
              From:{' '}
            </Text>
            <Text highlight>
              {fromDate ? moment(fromDate).format('MM/DD/YYYY') : 'Select a date'}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPressIn={() => {
            this.setState({ picking: 'to', isPickerVisible: true });
            tap.play();
          }}>
          <View horizontal center>
            <Icon name="ios-calendar-outline" highlight larger />
            <Text bold highlight>
              {' '}
              To:{' '}
            </Text>
            <Text highlight> {toDate ? moment(toDate).format('MM/DD/YYYY') : 'Select a date'}</Text>
          </View>
        </TouchableOpacity>
        <DateTimePicker
          isVisible={isPickerVisible}
          date={moment(picking === 'from' ? fromDate : toDate).toDate()}
          onConfirm={date => this.handleDatePicked(date)}
          onCancel={() => this.setState({ isPickerVisible: false })}
          maximumDate={moment().toDate()}
          minimumDate={moment()
            .subtract(30, 'd')
            .toDate()}
        />
      </View>
    );
  }
}
