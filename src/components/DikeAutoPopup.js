import React from 'react';
import { tap } from 'dike/utils/sounds';

import { View, Button, Text } from 'native-base';
import ScreenModal from './ScreenModal';

export default ({ isVisible, title, notice, onDismiss }) => {
  if (!isVisible) {
    return null;
  }
  return (
    <ScreenModal title={title} onDismiss={() => onDismiss()}>
      <Text padVertical>{notice}</Text>
      <View marginAround textAround>
        <Button
          fixedWidth
          onPress={() => {
            onDismiss();
            tap.play();
          }}>
          <Text>OK</Text>
        </Button>
      </View>
    </ScreenModal>
  );
};
