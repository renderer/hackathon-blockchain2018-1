import React from 'react';
import { View, Spinner } from 'native-base';

export default () => (
  <View center style={{ height: 300 }}>
    <Spinner />
  </View>
);
