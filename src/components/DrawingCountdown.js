import theme from 'dike/theme';
import { blip } from 'dike/utils/sounds';
import { connect } from 'react-redux';

import React, { Component } from 'react';

import { NetInfo, Animated, StyleSheet } from 'react-native';
import { H1 } from 'native-base';
import Countdown from 'react-countdown-now';

import { refreshApp } from 'dike/store/app/app.actions';

const styles = StyleSheet.create({
  text: { marginRight: theme.spacingUnit * 2 },
  animateText: {
    marginRight: theme.spacingUnit * 2,
    fontSize: theme.fontSizeBase * 1.8,
    fontWeight: 'bold'
  }
});

const NormalRenderer = ({ seconds, minutes, inProgress }) => (
  <H1
    bold
    style={[
      styles.text,
      { color: inProgress ? theme.dikeNumber.normal : theme.dikeNumber.special }
    ]}>{`${minutes}:${seconds}`}</H1>
);

const AnimatedText = ({ seconds, minutes, color }) => (
  <Animated.Text style={[styles.animateText, { color }]}>{`${minutes}:${seconds}`}</Animated.Text>
);

class DrawingCountdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animColor: new Animated.Value(1)
    };
  }

  handleRenderer(seconds, minutes) {
    const { inProgress } = this.props;
    if (
      inProgress &&
      parseInt(minutes, 10) === 0 &&
      parseInt(seconds, 10) < 10 &&
      parseInt(seconds, 10) > 0
    ) {
      blip.play();
      this.state.animColor.setValue(1);
      Animated.timing(this.state.animColor, { toValue: 0, duration: 1000 }).start();
      const color = this.state.animColor.interpolate({
        inputRange: [0, 1],
        outputRange: [theme.dikeNumber.special, theme.dikeNumber.normal]
      });
      return <AnimatedText seconds={seconds} minutes={minutes} color={color} />;
    }
    return <NormalRenderer seconds={seconds} minutes={minutes} inProgress={inProgress} />;
  }

  render() {
    const { date, onRefreshApp } = this.props;

    return [date].map(date => (
      <Countdown
        key={date}
        date={date}
        renderer={({ seconds, minutes }) => this.handleRenderer(seconds, minutes)}
        onComplete={() => {
          NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
              return;
            }
            onRefreshApp();
          });
        }}
      />
    ));
  }
}

export default connect(
  ({ drawing }) => {
    let date;
    let inProgress = false;
    if (drawing.drawingStatus.inProgress) {
      date = drawing.currentDrawing.endTime;
      inProgress = true;
    } else {
      date = drawing.drawingStatus.nextAt;
      inProgress = false;
    }
    return { date: date || new Date(), inProgress };
  },
  dispatch => ({
    onRefreshApp() {
      dispatch(refreshApp());
    }
  })
)(DrawingCountdown);
