import React from 'react';

import { StyleSheet } from 'react-native';
import { View, H1 } from 'native-base';

const styles = StyleSheet.create({
  container: { height: '100%' },
  modal: { width: '80%' }
});

export default ({
  children,
  onDismiss,
  containerStyle = {},
  modalStyle = {},
  title,
  disabledTouchEnd
}) => (
  <View
    center
    overlay
    style={[styles.container, { ...containerStyle }]}
    onTouchEnd={() => !disabledTouchEnd && onDismiss()}>
    <View bgMain round style={[styles.modal, { ...modalStyle }]}>
      {title && (
        <View center marginAround>
          <H1 bold>{title}</H1>
        </View>
      )}
      {children}
    </View>
  </View>
);
