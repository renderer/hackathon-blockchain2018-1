import theme from 'dike/theme';
import React from 'react';
import { connect } from 'react-redux';
import { setDikeResultPopupProps } from 'dike/store/app/app.actions';

import { Dimensions, StyleSheet, TouchableOpacity } from 'react-native';
import { View, Card, CardItem, Text, Body, Content, Button } from 'native-base';

import DikeNumber from './DikeNumber';
import ScreenModal from './ScreenModal';

import { padZero } from 'dike/utils/common/numbers';

const { width } = Dimensions.get('window');
const dikeNumberSize = Math.floor((width - 4 - theme.spacingUnit * 18) / 7);

const styles = StyleSheet.create({
  item: { width: `${100 / 7}%` },
  option: {
    width: 100,
    padding: theme.spacingUnit,
    borderRadius: 20,
    marginRight: theme.spacingUnit
  },
  body: { flexDirection: 'row' }
});

const RawResult = ({ result }) => (
  <View horizontal wrap>
    {result.map((number, i) => (
      <View center wrapItem style={styles.item} key={i}>
        <DikeNumber size={dikeNumberSize} number={number} special={i === 0} />
      </View>
    ))}
  </View>
);

const SortResult = ({ result }) => (
  <View horizontal wrap>
    <View center wrapItem style={styles.item}>
      <DikeNumber size={dikeNumberSize} number={result[0]} special={true} />
    </View>
    {result
      .slice(1, result.length)
      .sort((a, b) => a - b)
      .map((number, i) => (
        <View center wrapItem style={styles.item}>
          <DikeNumber size={dikeNumberSize} number={number} special={false} />
        </View>
      ))}
  </View>
);

const CategorizeResult = ({ result }) => {
  const numbers = {};

  result.forEach((number, i) => {
    const strResult = padZero(number);
    const group = strResult.charAt(0);
    if (!numbers[group]) {
      numbers[group] = [];
      numbers[group].push({ number, special: false, occurence: 1 });
    } else {
      let existIndex = -1;
      const isExist = numbers[group].some((numb, i) => {
        if (numb.number === number) {
          existIndex = i;
          return true;
        }
        return false;
      });
      if (isExist) {
        numbers[group][existIndex].occurence += 1;
      } else {
        numbers[group].push({ number, special: false, occurence: 1 });
      }
    }
  });

  return (
    <View fullWidth>
      <View horizontal wrap>
        <View center wrapItem style={styles.item}>
          <DikeNumber size={dikeNumberSize} number={result[0]} special={true} />
        </View>
      </View>
      {Object.keys(numbers)
        .sort((a, b) => b - a)
        .reverse()
        .map((group, i) => (
          <View horizontal wrap>
            {numbers[group]
              .sort((a, b) => b.number - a.number)
              .reverse()
              .map((numb, j) => (
                <View center wrapItem style={styles.item}>
                  <DikeNumber
                    size={dikeNumberSize}
                    number={numb.number}
                    special={numb.special}
                    occurence={numb.occurence}
                  />
                </View>
              ))}
          </View>
        ))}
    </View>
  );
};

const options = ['Raw', 'Sort', 'Categorize'];

const DikeResultPopup = ({ visible, result, activeType, onSetProps }) => {
  if (!visible) {
    return null;
  }
  return (
    <ScreenModal
      onDismiss={() => onSetProps({ visible: false })}
      containerStyle={{ width: '100%', height: '100%' }}
      modalStyle={{ width: '100%', height: '100%' }}
      disabledTouchEnd>
      <Content>
        <Card transparent>
          <CardItem borderBottom>
            <Body style={styles.body}>
              {options.map(opt => {
                const isActive = opt === activeType;
                return (
                  <TouchableOpacity key={opt} onPressIn={() => onSetProps({ activeType: opt })}>
                    <View style={styles.option} bgBlur={!isActive} bgDikeSpecial={isActive} center>
                      <Text bold>{opt}</Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </Body>
          </CardItem>
          <CardItem>
            {activeType === 'Raw' && <RawResult result={result} />}
            {activeType === 'Sort' && <SortResult result={result} />}
            {activeType === 'Categorize' && <CategorizeResult result={result} />}
          </CardItem>
          <CardItem center>
            <View half>
              <Button block primary onPress={() => onSetProps({ visible: false })}>
                <Text>Close</Text>
              </Button>
            </View>
          </CardItem>
        </Card>
      </Content>
    </ScreenModal>
  );
};

export default connect(
  ({ app }) => ({ ...app.dikeResultPopup }),
  dispatch => ({
    onSetProps(props) {
      dispatch(setDikeResultPopupProps(props));
    }
  })
)(DikeResultPopup);
