import theme from 'dike/theme';
import React from 'react';
import moment from 'moment';

import { StyleSheet } from 'react-native';
import { View, Text, H3 } from 'native-base';

const styles = StyleSheet.create({
  container: { alignItems: 'baseline' }
});

export default ({ time }) => {
  const ts = moment(time);
  return (
    <View horizontal style={styles.container}>
      <H3 bold>{ts.format('HH:mm:ss')}</H3>
      <Text style={{ marginLeft: theme.spacingUnit * 2 }}>{ts.format('MM/DD/YYYY')}</Text>
    </View>
  );
};
