import theme from 'dike/theme';
import React from 'react';
import { whoosh } from 'dike/utils/sounds';

import { View, Icon } from 'native-base';
import { TouchableWithoutFeedback, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

// import hexToRgba from 'dike/utils/common/hexToRgba';

const getIconName = (routeName, focused) => {
  let iconName;
  if (routeName === 'User') {
    iconName = `ios-person${focused ? '' : '-outline'}`;
  } else if (routeName === 'Shop') {
    iconName = `ios-people${focused ? '' : '-outline'}`;
  } else if (routeName === 'DrawingHistory') {
    iconName = `ios-clipboard${focused ? '' : '-outline'}`;
  } else if (routeName === 'Ranking') {
    iconName = `ios-trophy${focused ? '' : '-outline'}`;
  }
  return iconName;
};

const styles = StyleSheet.create({
  bottomBar: {
    height: 49,
    width: '100%',
    flexDirection: 'row',
    shadowOffset: {
      width: 0,
      height: -2
    },
    shadowColor: '#000000',
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 4,
    borderTopWidth: StyleSheet.hairlineWidth
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default ({ navigationState, navigation, jumpToIndex }) => (
  <LinearGradient colors={[theme.bbGradient.start, theme.bbGradient.end]} style={styles.bottomBar}>
    {navigationState.routes.map((route, i) => {
      const focusedIndex = navigation.state.index;
      return (
        <TouchableWithoutFeedback
          key={i}
          onPressIn={() => {
            if (i !== focusedIndex) {
              navigation.navigate(route.routeName, { date: new Date() });
              whoosh.play();
            }
          }}>
          <View style={styles.item}>
            <Icon
              name={getIconName(route.routeName, i === navigationState.index)}
              style={{ color: i === focusedIndex ? theme.text.error : theme.text.main }}
            />
          </View>
        </TouchableWithoutFeedback>
      );
    })}
  </LinearGradient>
);
