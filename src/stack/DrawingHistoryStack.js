import React from 'react';
import { TouchableOpacity } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Icon } from 'native-base';

import { DrawingDetailScreen } from 'dike/screen/drawingDetail';
import { DrawingHistoryScreen } from 'dike/screen/drawingHistory';
import { DrawingOverviewScreen } from 'dike/screen/drawingOverview';

import DrawingDetailHeader from 'dike/components/DrawingDetailHeader';

import theme from 'dike/theme';

export default StackNavigator({
  DrawingHistory: {
    screen: DrawingHistoryScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Drawing History',
      headerRight: (
        <TouchableOpacity onPressIn={() => navigation.navigate('DrawingOverview')}>
          <Icon name="ios-stats" />
        </TouchableOpacity>
      ),
      headerStyle: { backgroundColor: theme.primary.main, paddingRight: theme.spacingUnit * 2 },
      headerTitleStyle: { color: theme.text.main },
      headerTintColor: theme.text.main
    })
  },
  DrawingDetail: {
    screen: DrawingDetailScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <DrawingDetailHeader />,
      headerStyle: { backgroundColor: theme.primary.main },
      headerTintColor: theme.text.main
    })
  },
  DrawingOverview: {
    screen: DrawingOverviewScreen,
    navigationOptions: {
      title: 'Drawing Overview',
      headerStyle: { backgroundColor: theme.primary.main },
      headerTintColor: theme.text.main
    }
  }
});
