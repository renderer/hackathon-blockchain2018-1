import React from 'react';
import { StackNavigator } from 'react-navigation';

import { ProfileScreen } from 'dike/screen/profile';
import EditProfileScreen from 'dike/screen/profile/EditProfileScreen.container';
import PrivacyScreen from 'dike/screen/setting/PrivacyScreen.container';
import PeopleScreen from 'dike/screen/people/PeopleScreen.container';
import { TransactionHistoryScreen } from 'dike/screen/transactionHistory';
import { DrawingDetailScreen } from 'dike/screen/drawingDetail';
import { WalletScreen } from 'dike/screen/wallet';
import { CurrentDrawingDetailScreen } from 'dike/screen/currentDrawingDetail';
import { SettingScreen, ReportIssueScreen, ChangePasswordScreen } from 'dike/screen/setting';

import ProfileHeader from 'dike/screen/profile/ProfileHeader.container';
import DrawingCountdownHeader from 'dike/components/DrawingCountdownHeader';
import DrawingCountdown from 'dike/components/DrawingCountdown';
import DrawingDetailHeader from 'dike/components/DrawingDetailHeader';
import ProfileHeaderRight from 'dike/screen/profile/ProfileHeaderRight.component';

import theme from 'dike/theme';
import { Text } from 'native-base';

export default StackNavigator(
  {
    Profile: {
      screen: ProfileScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: <ProfileHeader />,
        headerRight: (
          <ProfileHeaderRight onEditUserProfile={() => navigation.navigate('Setting')} />
        ),
        headerStyle: { backgroundColor: theme.primary.main }
      })
    },
    EditProfile: {
      screen: EditProfileScreen,
      navigationOptions: () => ({
        headerTitle: <Text>Edit Profile</Text>,
        headerStyle: { backgroundColor: theme.primary.main }
      })
    },
    PeopleScreen: {
      screen: PeopleScreen,
      navigationOptions: () => ({
        headerTitle: <Text>Profile</Text>,
        headerStyle: { backgroundColor: theme.primary.main }
      })
    },
    PrivacyScreen: {
      screen: PrivacyScreen,
      navigationOptions: () => ({
        headerTitle: <Text>Update privacy</Text>,
        headerStyle: { backgroundColor: theme.primary.main }
      })
    },
    DrawingDetail: {
      screen: DrawingDetailScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: <DrawingDetailHeader />,
        headerStyle: { backgroundColor: theme.primary.main },
        headerTintColor: theme.text.main
      })
    },
    Setting: {
      screen: SettingScreen,
      navigationOptions: {
        title: 'Settings',
        headerStyle: { backgroundColor: theme.primary.main },
        headerTitleStyle: { color: theme.text.main },
        headerTintColor: theme.text.main
      }
    },
    Transactions: {
      screen: TransactionHistoryScreen,
      navigationOptions: {
        title: 'Transaction History',
        headerStyle: { backgroundColor: theme.primary.main },
        headerTitleStyle: { color: theme.text.main },
        headerTintColor: theme.text.main
      }
    },
    CurrentDrawingDetail: {
      screen: CurrentDrawingDetailScreen,
      navigationOptions: () => ({
        headerTitle: <DrawingCountdownHeader />,
        headerRight: <DrawingCountdown />,
        headerStyle: { backgroundColor: theme.primary.main },
        headerTintColor: theme.text.main
      })
    },
    Wallet: {
      screen: WalletScreen,
      navigationOptions: {
        title: 'Wallet',
        headerStyle: { backgroundColor: theme.primary.main },
        headerTitleStyle: { color: theme.text.main },
        headerTintColor: theme.text.main
      }
    },
    ChangePassword: {
      screen: ChangePasswordScreen,
      navigationOptions: {
        title: 'Change password',
        headerStyle: { backgroundColor: theme.primary.main },
        headerTitleStyle: { color: theme.text.main },
        headerTintColor: theme.text.main
      }
    },
    ReportIssue: {
      screen: ReportIssueScreen,
      navigationOptions: {
        title: 'Report Issue',
        headerStyle: { backgroundColor: theme.primary.main },
        headerTitleStyle: { color: theme.text.main },
        headerTintColor: theme.text.main
      }
    }
  },
  {
    initialRouteName: 'Profile'
  }
);
