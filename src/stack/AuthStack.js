import { StackNavigator } from 'react-navigation';

import { LoginScreen } from 'dike/screen/login';
import { RegisterScreen } from 'dike/screen/register';
import {
  ForgotPasswordScreen,
  SetPasswordScreen,
  SetPasswordVerifyCodeScreen
} from 'dike/screen/forgotPassword';

export default StackNavigator(
  {
    Login: {
      screen: LoginScreen
    },
    Register: {
      screen: RegisterScreen,
      path: 'register/:ref'
    },
    ForgotPassword: {
      screen: ForgotPasswordScreen
    },
    SetPassword: {
      screen: SetPasswordScreen,
      path: 'setPassword/:code'
    },
    SetPasswordVerifyCode: {
      screen: SetPasswordVerifyCodeScreen
    }
  },
  {
    initialRouteName: 'Login',
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
);
