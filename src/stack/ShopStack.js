import { StackNavigator } from 'react-navigation';
import { ShopScreen } from 'dike/screen/shop';

import theme from 'dike/theme';

export default StackNavigator({
  Shop: {
    screen: ShopScreen,
    navigationOptions: {
      headerTitle: 'Shops',
      headerStyle: { backgroundColor: theme.primary.main },
      headerTintColor: theme.text.main
    }
  }
});
