import React from 'react';
import { StackNavigator } from 'react-navigation';

import { DrawingDetailScreen } from 'dike/screen/drawingDetail';
import { GameScreen } from 'dike/screen/game';
import { PlaceBetsScreen } from 'dike/screen/placeBets';

import DrawingCountdownHeader from 'dike/components/DrawingCountdownHeader';
import DrawingCountdown from 'dike/components/DrawingCountdown';
import DrawingDetailHeader from 'dike/components/DrawingDetailHeader';

import theme from 'dike/theme';

export default StackNavigator(
  {
    Game: {
      screen: GameScreen
    },
    PlaceBets: {
      screen: PlaceBetsScreen
    },
    DrawingDetail: {
      screen: DrawingDetailScreen,
      navigationOptions: ({ navigation }) => ({
        headerTitle: <DrawingDetailHeader />,
        headerStyle: { backgroundColor: theme.primary.main },
        headerTintColor: theme.text.main
      })
    }
  },
  {
    initialRouteName: 'Game',
    navigationOptions: ({ navigation }) => ({
      headerTitle: <DrawingCountdownHeader />,
      headerRight: <DrawingCountdown />,
      headerStyle: { backgroundColor: theme.primary.main },
      headerTintColor: theme.text.main
    })
  }
);
