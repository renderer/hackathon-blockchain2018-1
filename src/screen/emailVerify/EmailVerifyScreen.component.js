import React from 'react';

import PinInputScreen from 'dike/components/PinInputScreen';

const description =
  'A verification code has been sent to your email. To complete your sign in, please enter the code you received.';

export default ({ hasError, isLoading, onVerify, onHideError, onResend }) => (
  <PinInputScreen
    description={description}
    showError={hasError}
    onHideError={() => onHideError()}
    onResend={() => onResend()}
    onCodeVerify={code => onVerify(code)}
    isLoading={isLoading}
  />
);
