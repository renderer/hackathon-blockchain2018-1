import React from 'react';
import {
  Text
 } from 'native-base';
 import { Platform } from 'react-native';

export default ({ username }) => (
  <Text
    style={{
      fontSize: Platform.OS === 'ios' ? 17 : 20,
      fontWeight: Platform.OS === 'ios' ? '700' : '500',
      textAlign: Platform.OS === 'ios' ? 'center' : 'left',
      marginHorizontal: 16
    }}>
    {username ? `${username}'s Profile` : 'Your Profile'} 
  </Text>
);