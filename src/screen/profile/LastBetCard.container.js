import { connect } from 'react-redux';

import LastBetCard from './LastBetCard.component';
import { getLastBet } from 'dike/store/drawing/drawing.actions';

export default connect(
  ({ drawing, app }) => ({ ...drawing.lastBet, isLoading: app.isLastBetLoading }),
  dispatch => ({
    onGetLastBet() {
      dispatch(getLastBet());
    }
  })
)(LastBetCard);
