import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Text, View } from 'native-base';



export default ({ onEditUserProfile }) => {
  return (
    <View style={{ flexDirection: "row" }}>
      <TouchableOpacity onPress={() => onEditUserProfile && onEditUserProfile()}
      >
        <Text highlight bold right>
          Settings
        </Text>
      </TouchableOpacity>
    </View>
  );
}
