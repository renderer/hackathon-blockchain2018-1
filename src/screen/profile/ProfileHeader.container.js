import { connect } from 'react-redux';
import ProfileHeader from './ProfileHeader.component';

export default connect(({ user }) => ({
  username: user.username
}))(ProfileHeader);