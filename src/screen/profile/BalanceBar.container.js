import { connect } from 'react-redux';
import BalanceBar from './BalanceBar.component';

export default connect(({ user }) => ({
  balance: user.balance,
  activated: user.activated
}))(BalanceBar);
