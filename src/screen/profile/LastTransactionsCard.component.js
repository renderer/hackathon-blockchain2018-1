import React from 'react';
import { FlatList } from 'react-native';
import { View, Text, Card, CardItem, H3 } from 'native-base';

import TransactionItem from 'dike/components/TransactionItem';

export default ({ transactions, onTransactionPress, onSeeMorePress, onNotActivatedPress }) => {
  const notActivated = !transactions || transactions.length < 1;
  return (
    <Card bgSemiDark>
      <CardItem header>
        <H3 bold>Transaction History</H3>
      </CardItem>
      <CardItem button={notActivated} onPress={() => notActivated && onNotActivatedPress()}>
        {notActivated && (
          <View horizontal wrap>
            <Text>No recent transactions recorded.</Text>
            <Text highlight> Press here</Text>
            <Text> to go to your wallet. You can find your wallet's address there</Text>
          </View>
        )}
        {transactions &&
          transactions.length > 1 && (
            <FlatList
              data={transactions}
              keyExtractor={item => item._id}
              renderItem={({ item }) => (
                <TransactionItem
                  transaction={item}
                  onPress={transaction => onTransactionPress(transaction)}
                />
              )}
            />
          )}
      </CardItem>

      {transactions &&
        transactions.length > 4 && (
          <CardItem button footer onPress={() => onSeeMorePress()}>
            <View fullWidth center>
              <Text highlight>See more</Text>
            </View>
          </CardItem>
        )}
    </Card>
  );
};
