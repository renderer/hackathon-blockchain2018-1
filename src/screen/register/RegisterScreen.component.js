import { tap } from 'dike/utils/sounds';
import React, { Component } from 'react';

import { Text, Item, Input, View, Button, H1, Form } from 'native-base';

import ScreenOverlayLoading from 'dike/components/ScreenOverlayLoading';
import DikeBackground from 'dike/components/DikeBackground';
import ItemInputPassword from 'dike/components/ItemInputPassword';
import { validateEmail, validateUsername, validatePassword } from 'dike/utils/common/validators';

export default class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      username: '',
      error: { email: false, password: false, username: false },
      refererCode: ''
    };
  }

  handleSubmit() {
    const { email, password, username } = this.state;

    const isValidEmail = validateEmail(email);
    const isValidPassword = validatePassword(password);
    const isValidUsername = validateUsername(username);

    if (!isValidPassword || !isValidEmail || !isValidUsername) {
      this.setState({
        error: { email: !isValidEmail, password: !isValidPassword, username: !isValidUsername }
      });
    } else {
      this.props.onClearError();
      this.setState({
        error: { email: false, password: false, username: false }
      });
      const { navigation } = this.props;
      const ref = navigation.getParam('ref');
      const formBody = { email, password, username };
      if (ref) {
        formBody.refererCode = ref;
      }
      this.props.onRegister(formBody);
    }
  }

  render() {
    const { error, email, password, username } = this.state;
    const { navigation, registerError, isLoading } = this.props;

    return (
      <DikeBackground>
        <View full center>
          <View authScreenWrapper>
            <H1 bold>Welcome to Dike</H1>
            <Text endDescription>
              Register an email address to keep your account data backed up
            </Text>
            {registerError &&
              Object.keys(registerError).map(key => (
                <Text error small>
                  *{registerError[key]}
                </Text>
              ))}

            <Form full>
              <Item error={error.email} noLabel>
                <Input
                  keyboardType="email-address"
                  placeholder="Email Address"
                  value={email}
                  autoCapitalize="none"
                  onChangeText={v => {
                    const nextState = { email: v };
                    if (error.email) {
                      error.email = !validateEmail(v);
                      nextState.error = error;
                    }
                    this.setState(nextState);
                  }}
                />
              </Item>
              {error.email && (
                <Text error small>
                  *Invalid email
                </Text>
              )}

              <Item error={error.username} noLabel>
                <Input
                  placeholder="Username"
                  value={username}
                  autoCapitalize="none"
                  onChangeText={v => {
                    const nextState = { username: v };
                    if (error.username) {
                      error.username = !validateUsername(v);
                      nextState.error = error;
                    }
                    this.setState(nextState);
                  }}
                />
              </Item>
              {error.username && (
                <Text error small>
                  *Invalid username
                </Text>
              )}

              <ItemInputPassword
                value={password}
                error={error.password}
                onChangeText={v => {
                  const nextState = { password: v };
                  if (error.password) {
                    error.password = !validatePassword(v);
                    nextState.error = error;
                  }
                  this.setState(nextState);
                }}
              />
            </Form>

            <View fullWidth center>
              <View half fullFormFooter>
                <Button
                  block
                  primary
                  onPress={() => {
                    this.handleSubmit();
                    tap.play();
                  }}>
                  <Text>Sign up</Text>
                </Button>
                <Button
                  block
                  info
                  onPress={() => {
                    navigation.goBack();
                    tap.play();
                  }}>
                  <Text>Cancel</Text>
                </Button>
              </View>
            </View>
          </View>
        </View>

        {isLoading && <ScreenOverlayLoading />}
      </DikeBackground>
    );
  }
}
