import React, { Component } from 'react';
import { Content } from 'native-base';

import DikeBackground from 'dike/components/DikeBackground';
import ScreenOverlayLoading from 'dike/components/ScreenOverlayLoading';
import ConfirmDialog from 'dike/components/ConfirmDialog';
import UserInfo from 'dike/components/UserInfo';

import AutoPopup from './AutoPopup.container';
import EthCard from './EthCard.container';
import DikeCard from './DikeCard.component';
import ResetBalanceCard from './ResetBalanceCard.component';

import { tap } from 'dike/utils/sounds';

export default class WalletScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { isConfirmingReset: false };
  }

  render() {
    const { isLoading, onResetDemoBalance, activated } = this.props;
    const { isConfirmingReset } = this.state;
    return (
      <DikeBackground>
        <Content>
          <UserInfo showBalance ViewProps={{ paddingContent: true }} />
          <ResetBalanceCard
            onResetPress={() => {
              this.setState({ isConfirmingReset: true });
              tap.play();
            }}
            activated={activated}
          />
          <EthCard />
          <DikeCard />
        </Content>
        <AutoPopup />
        {isLoading && <ScreenOverlayLoading />}
        {isConfirmingReset && (
          <ConfirmDialog
            onDismiss={() => this.setState({ isConfirmingReset: false })}
            onSubmit={() => onResetDemoBalance()}
            title="Are you sure?"
            notice="Your balance will be reset back to Đ1,000."
          />
        )}
      </DikeBackground>
    );
  }
}
