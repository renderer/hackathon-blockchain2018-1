import { connect } from 'react-redux';
import DikeAutoPopup from 'dike/components/DikeAutoPopup';

import { setWalletPopupProps } from 'dike/store/app/app.actions';

export default connect(
  ({ app }) => ({ ...app.walletPopup }),
  dispatch => ({
    onDismiss() {
      dispatch(setWalletPopupProps({ isVisible: false }));
    }
  })
)(DikeAutoPopup);
