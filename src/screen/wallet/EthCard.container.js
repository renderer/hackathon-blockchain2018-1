import { connect } from 'react-redux';
import EthCard from './EthCard.component';

export default connect(({ user }) => ({
  ethDepositAddress: user.ethDepositAddress
}))(EthCard);
