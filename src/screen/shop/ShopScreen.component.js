import React from 'react';
import { Text, View } from 'native-base';
import DikeBackground from 'dike/components/DikeBackground';

export default (ShopScreen = () => {
  return (
    <DikeBackground>
      <View center style={{ marginTop: 24 }}>
        <Text>No service providers available!</Text>
      </View>
    </DikeBackground>
  );
});
