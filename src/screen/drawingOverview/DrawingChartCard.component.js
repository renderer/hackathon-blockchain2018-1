import theme from 'dike/theme';
import React, { Component } from 'react';
import { Dimensions, StyleSheet } from 'react-native';

import { Card, CardItem, View, H3, Left } from 'native-base';

import CardLoading from 'dike/components/CardLoading';
import DikeNumber from 'dike/components/DikeNumber';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  item: { width: `${100 / 5}%` }
});

class DrawingChartCard extends Component {
  componentDidMount() {
    const { drawingChart, onGetDrawingChart } = this.props;
    if (!drawingChart) {
      onGetDrawingChart();
    }
  }

  render() {
    const { drawingChart } = this.props;
    if (!drawingChart) {
      return <CardLoading CardProps={{ first: true, bgMain: true }} />;
    }
    return (
      <Card bgMain first>
        <CardItem header borderBottom>
          <Left>
            <H3 bold>Recent special numbers</H3>
          </Left>
        </CardItem>
        <CardItem>
          <View horizontal wrap wrapContainer>
            {drawingChart.lastSpecials.map((number, i) => (
              <View center style={styles.item} wrapItem key={i}>
                <DikeNumber
                  number={number}
                  size={Math.floor((width - 4 - theme.spacingUnit * 14) / 5)}
                  special={true}
                />
              </View>
            ))}
          </View>
        </CardItem>
        <CardItem header borderBottom>
          <Left>
            <H3 bold>Top hitting numbers</H3>
          </Left>
        </CardItem>
        <CardItem>
          <View horizontal wrap wrapContainer>
            {drawingChart.topHitNumbers.map((number, i) => (
              <View center style={styles.item} wrapItem key={i}>
                <DikeNumber
                  number={number}
                  size={Math.floor((width - 4 - theme.spacingUnit * 14) / 5)}
                  special={false}
                />
              </View>
            ))}
          </View>
        </CardItem>
        <CardItem header borderBottom>
          <Left>
            <H3 bold>Top missing numbers</H3>
          </Left>
        </CardItem>
        <CardItem>
          <View horizontal wrap>
            {drawingChart.topMissNumbers.map((number, i) => (
              <View center style={styles.item} wrapItem key={i}>
                <DikeNumber
                  number={number}
                  size={Math.floor((width - 4 - theme.spacingUnit * 14) / 5)}
                  lost={true}
                />
              </View>
            ))}
          </View>
        </CardItem>
      </Card>
    );
  }
}

export default DrawingChartCard;
