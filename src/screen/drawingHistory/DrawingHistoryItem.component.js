import theme from 'dike/theme';
import React from 'react';

import { View, Text, Card, CardItem, Left, Right, Icon } from 'native-base';

import TimeRow from 'dike/components/TimeRow';
import DikeResult from 'dike/components/DikeResult';

const CollapsedItem = ({ drawing, onPress }) => (
  <Card bgMain smallestMargin>
    <CardItem header button onPressOut={() => onPress(drawing)}>
      <Left>
        <TimeRow time={drawing.endTime} />
      </Left>
      <Right>
        <View horizontal center>
          <Text style={{ marginRight: drawing.participated ? theme.spacingUnit * 2 : 0 }}>
            #{drawing.serial}
          </Text>
          {drawing.participated && <Icon name="ios-checkmark-circle" success medium />}
        </View>
      </Right>
    </CardItem>
  </Card>
);

const ExpandedItem = ({ drawing, onPress, onNavigate }) => (
  <Card transparent smallestMargin>
    <CardItem header button borderBottom onPressOut={() => onPress(drawing)}>
      <Left>
        <TimeRow time={drawing.endTime} />
      </Left>
      <Right>
        <View horizontal center>
          <Text style={{ marginRight: drawing.participated ? theme.spacingUnit * 2 : 0 }}>
            #{drawing.serial}
          </Text>
          {drawing.participated && <Icon name="ios-checkmark-circle" success medium />}
        </View>
      </Right>
    </CardItem>
    <CardItem>
      <DikeResult result={drawing.result} />
    </CardItem>
    <CardItem endFlexBody footer button onPress={() => onNavigate(drawing)}>
      <View>
        <Text highlight>View Detail</Text>
      </View>
    </CardItem>
  </Card>
);

export default ({ drawing, onPress, onNavigate, expandId }) =>
  expandId === drawing.id ? (
    <ExpandedItem onPress={dr => onPress(dr)} drawing={drawing} onNavigate={dr => onNavigate(dr)} />
  ) : (
    <CollapsedItem onPress={dr => onPress(dr)} drawing={drawing} />
  );
