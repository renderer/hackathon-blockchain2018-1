import DrawingHistoryScreen from './DrawingHistoryScreen.component';
import { connect } from 'react-redux';

import { getDrawingHistory } from 'dike/store/drawing/drawing.actions';

export default connect(
  ({ drawing, app }) => ({
    date: drawing.drawingHistory.date,
    participated: drawing.drawingHistory.participated,
    drawings: drawing.drawingHistory.drawings,
    isLoading: app.isDrawingHistoryScreenLoading
  }),
  dispatch => ({
    onGetDrawingHistory({ date, participated }) {
      dispatch(getDrawingHistory({ date, participated }));
    }
  })
)(DrawingHistoryScreen);
