import { connect } from 'react-redux';
import RankingScreen from './RankingScreen.component';

import { getRanking } from 'dike/store/user/user.actions';

export default connect(
  ({ user }) => ({
    data: user.rankingData
  }),
  dispatch => ({
    onGetRanking() {
      dispatch(getRanking());
    }
  })
)(RankingScreen);
