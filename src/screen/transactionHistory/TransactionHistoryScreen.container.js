import moment from 'moment';
import { connect } from 'react-redux';
import TransactionHistoryScreen from './TransactionHistoryScreen.component';

import { getTransactionHistory, changeTransactionHistoryDate } from 'dike/store/user/user.actions';

export default connect(
  ({ user, drawing, app }) => ({
    isLoading: app.isTransactionsScreenLoading,
    transactions: user.transactionHistory.transactions,
    fromDate: user.transactionHistory.fromDate,
    toDate: user.transactionHistory.toDate,
    currentDrawingId: drawing.currentDrawing ? drawing.currentDrawing.id : null
  }),
  dispatch => ({
    onDateChange({ fromDate, toDate }) {
      dispatch(changeTransactionHistoryDate({ fromDate, toDate }));
      dispatch(
        getTransactionHistory({
          fromDate: moment(fromDate).unix(),
          toDate: moment(toDate).unix()
        })
      );
    }
  })
)(TransactionHistoryScreen);
