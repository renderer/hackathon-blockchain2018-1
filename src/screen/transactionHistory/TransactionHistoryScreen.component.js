import React, { Component } from 'react';

import { FlatList, StyleSheet } from 'react-native';
import { View } from 'native-base';

import DateRangeSelector from 'dike/components/DateRangeSelector';
import TransactionItem from 'dike/components/TransactionItem';
import DikeBackground from 'dike/components/DikeBackground';
import NoHistoryNotice from 'dike/components/NoHistoryNotice';
import ScreenLoading from 'dike/components/ScreenLoading';

import { tap } from 'dike/utils/sounds';
import theme from 'dike/theme';

const styles = StyleSheet.create({
  flatList: { marginLeft: theme.spacingUnit * 4, paddingRight: theme.spacingUnit * 4 }
});

export default class TransactionHistoryScreen extends Component {
  constructor(props) {
    super(props);
    const { fromDate, toDate } = this.props;
    this.state = { fromDate, toDate };
  }

  handleDateChange({ fromDate, toDate }) {
    this.setState({ fromDate, toDate });
    this.props.onDateChange({ fromDate, toDate });
  }

  handleTransactionPress(transaction) {
    const { navigation, currentDrawingId } = this.props;

    if (!transaction.data || !transaction.data.drawingId) {
      return;
    }
    if (transaction.data.drawingId === currentDrawingId) {
      tap.play();
      navigation.navigate('CurrentDrawingDetail', {
        drawingId: currentDrawingId
      });
    } else {
      tap.play();
      navigation.navigate('DrawingDetail', {
        drawingId: transaction.data.drawingId
      });
    }
  }

  componentDidMount() {
    const { fromDate, toDate, onDateChange } = this.props;
    onDateChange({ fromDate, toDate });
  }

  renderNotice() {
    const { isLoading, transactions } = this.props;
    if (!isLoading) {
      if (!transactions || transactions.length < 1) {
        return <NoHistoryNotice />;
      }
    }
    return null;
  }

  render() {
    const { transactions, isLoading } = this.props;
    const { fromDate, toDate } = this.state;

    return (
      <DikeBackground>
        <View fullWidth>
          <DateRangeSelector
            ViewProps={{ marginAround: true }}
            defaultValue={{ fromDate, toDate }}
            onChange={({ fromDate, toDate }) => this.handleDateChange({ fromDate, toDate })}
          />
        </View>
        {isLoading && <ScreenLoading />}
        {this.renderNotice()}
        {!isLoading &&
          transactions &&
          transactions.length > 0 && (
            <FlatList
              style={styles.flatList}
              data={transactions}
              keyExtractor={item => item._id}
              renderItem={({ item }) => (
                <TransactionItem
                  transaction={item}
                  onPress={transaction => this.handleTransactionPress(transaction)}
                />
              )}
            />
          )}
      </DikeBackground>
    );
  }
}
