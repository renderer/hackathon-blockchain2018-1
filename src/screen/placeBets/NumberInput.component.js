import React from 'react';
import { View, Text } from 'native-base';

import BlinkingView from 'dike/components/BlinkingView';

const isValidValue = value => value && value.length === 2;

export default ({ isActive, value, size = 40, isSpecial = false }) => {
  const fontSize = size / 2.5;
  return (
    <View
      horizontal
      center
      bgBlur={isActive || !isValidValue(value)}
      bgDikeSpecial={!isActive && isSpecial && isValidValue(value)}
      bgDikeNormal={!isActive && !isSpecial && isValidValue(value)}
      style={{ borderRadius: size / 2, width: size, height: size }}>
      {isActive &&
        !value && [
          <BlinkingView key="input">
            <Text primary bold style={{ fontSize }}>
              _
            </Text>
          </BlinkingView>,
          <Text bold key="placeholder" style={{ fontSize }}>
            {' '}
          </Text>
        ]}
      {isActive &&
        !!value && [
          <Text primary bold style={{ fontSize }} key="value">
            {value}
          </Text>,
          <BlinkingView key="input">
            <Text primary bold style={{ fontSize }}>
              _
            </Text>
          </BlinkingView>
        ]}
      {!isActive &&
        isValidValue(value) && (
          <View center>
            <Text bold style={{ fontSize }}>
              {value}
            </Text>
            {isSpecial && <Text style={{ fontSize: size / 6 }}>Special</Text>}
          </View>
        )}
    </View>
  );
};
