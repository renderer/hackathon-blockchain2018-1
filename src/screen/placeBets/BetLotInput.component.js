import theme from 'dike/theme';
import React from 'react';

import { StyleSheet } from 'react-native';
import { View } from 'native-base';
import NumberInput from './NumberInput.component';

import withDimensions from 'dike/utils/hoc/withDimensions';

const styles = StyleSheet.create({
  inputItem: { width: `${100 / 5}%` }
});

const indexes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

const BetLotInput = ({ isFocus, value = [], isSpecial = false, dimensions, activeIndex }) => (
  <View horizontal wrap wrapContainer>
    {indexes.map(index => (
      <View wrapItem center style={styles.inputItem} key={index}>
        <NumberInput
          size={(dimensions.width - theme.spacingUnit * 8) / 5}
          key={index}
          value={value[index]}
          isActive={isFocus && index === activeIndex}
          isSpecial={isSpecial}
        />
      </View>
    ))}
  </View>
);

export default withDimensions()(BetLotInput);
