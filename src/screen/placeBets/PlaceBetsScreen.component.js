import React, { Component } from 'react';
import theme from 'dike/theme';
import BigNumber from 'bignumber.js';

import { TouchableOpacity, StyleSheet } from 'react-native';
import { View, Text, Content, Button, Card, CardItem, Left, Right, H3 } from 'native-base';

import DikeBackground from 'dike/components/DikeBackground';
import ScreenOverlayLoading from 'dike/components/ScreenOverlayLoading';
import ConfirmDialog from 'dike/components/ConfirmDialog';
import Keyboard from 'dike/components/Keyboard';

import BetLotCard from './BetLotCard.component';
import StakeDialog from './StakeDialog.component';
import AutoPopup from './AutoPopup.container';

import { tap, chip } from 'dike/utils/sounds';
import withDimensions from 'dike/utils/hoc/withDimensions';

const styles = StyleSheet.create({
  addBetWrapper: { height: theme.spacingUnit * 24 },
  addBetCard: { marginBottom: 0 },
  addBetLine: { flexDirection: 'row' }
});

const calcLotTotal = (numbers, stake) => {
  if (!stake) {
    return 0;
  }
  const validBets = numbers.filter(number => number && number.length === 2);
  return stake * validBets.length;
};

const calcTotalStake = betLots => {
  let total = 0;
  betLots.forEach(betLot => {
    total += betLot.lotTotal;
  });
  return total;
};

const getActiveIndex = value => {
  const lastValue = value[value.length - 1];
  if (lastValue && lastValue.length === 1) {
    return value.length - 1;
  } else {
    return value.length;
  }
};

class PlaceBetsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      betLots: [{ numbers: [], stake: 0, special: false, activeIndex: 0, lotTotal: 0 }],
      totalStake: 0,
      focusLotIndex: 0,
      isStaking: false,
      stakingValue: '',
      stakingMultiply: 0,
      stakingLotIndex: 0,
      isConfirming: false
    };
  }

  handleBetInput(value) {
    const { betLots, focusLotIndex } = this.state;

    if (value === 'Normal') {
      betLots[focusLotIndex].special = false;
      return this.setState({ betLots });
    }
    if (value === 'Special') {
      betLots[focusLotIndex].special = true;
      return this.setState({ betLots });
    }

    const { numbers } = betLots[focusLotIndex];
    let editingNumber = numbers[numbers.length - 1];

    if (value === 'BackSpace') {
      if (editingNumber && editingNumber.length > 1) {
        numbers[numbers.length - 1] = editingNumber.slice(0, 1);
      } else {
        numbers.pop();
      }
      const newBetLots = betLots.map((betLot, betLotIndex) => {
        if (betLotIndex !== focusLotIndex) {
          return betLot;
        }
        betLot.numbers = numbers;
        betLot.activeIndex = getActiveIndex(numbers);
        betLot.lotTotal = calcLotTotal(numbers, betLot.stake);
        return betLot;
      });
      this.setState({
        betLots: newBetLots,
        totalStake: calcTotalStake(newBetLots)
      });
      return;
    }

    if (!editingNumber) {
      numbers.push(value);
    } else if (editingNumber.length === 1) {
      numbers[numbers.length - 1] = editingNumber + value;
    } else {
      numbers.push(value);
    }
    const newBetLots = betLots.map((betLot, betLotIndex) => {
      if (betLotIndex !== focusLotIndex) {
        return betLot;
      }
      betLot.numbers = numbers.slice(0, 10);
      betLot.activeIndex = getActiveIndex(numbers);
      betLot.lotTotal = calcLotTotal(numbers, betLot.stake);
      return betLot;
    });
    this.setState({
      betLots: newBetLots,
      totalStake: calcTotalStake(newBetLots)
    });
  }

  handleAddBetLot() {
    const { betLots } = this.state;
    if (
      betLots[betLots.length - 1].numbers[0] &&
      betLots[betLots.length - 1].numbers[0].length === 2
    ) {
      betLots.push({ numbers: [], stake: 0, special: false, activeIndex: 0, lotTotal: 0 });
      this.setState({ betLots, focusLotIndex: betLots.length - 1 }, () => {
        setTimeout(() => this.content._root.scrollToEnd({ animated: true }), 0);
      });
    }
  }

  handleRemoveBetLot(lotIndex) {
    const { betLots } = this.state;
    if (betLots.length === 1) {
      this.setState({
        betLots: [{ numbers: [], stake: 0, special: false, activeIndex: 0, lotTotal: 0 }]
      });
    } else {
      betLots.splice(lotIndex, 1);
      this.setState({ betLots, focusLotIndex: betLots.length - 1 });
    }
  }

  handleStakePress(lotIndex) {
    const { betLots } = this.state;
    const validBets = betLots[lotIndex].numbers.filter(number => number && number.length === 2);
    this.setState({
      isStaking: true,
      stakingLotIndex: lotIndex,
      stakingMultiply: validBets.length,
      stakingValue: betLots[lotIndex].stake.toString()
    });
  }

  handleStakeInput(value) {
    const { stakingValue } = this.state;
    if (value === 'BackSpace') {
      if (!stakingValue) {
        return;
      }
      return this.setState({ stakingValue: stakingValue.slice(0, -1) });
    }
    this.setState({ stakingValue: stakingValue + value });
  }

  handleStakeSubmit() {
    const { betLots, stakingLotIndex, stakingValue } = this.state;
    let stakedValue = stakingValue ? parseInt(stakingValue, 10) : 0;
    betLots[stakingLotIndex].stake = stakedValue;
    betLots[stakingLotIndex].lotTotal = calcLotTotal(betLots[stakingLotIndex].numbers, stakedValue);

    this.setState({
      betLots,
      stakingValue: '',
      totalStake: calcTotalStake(betLots),
      isStaking: false
    });
  }

  handlePlaceBetPress() {
    const isValidBet = this.validateBets();
    if (!isValidBet) {
      return;
    }
    this.setState({ isConfirming: true });
  }

  validateBets() {
    const { betLots } = this.state;
    let errorIndex = undefined;
    let hasValidBet = false;
    for (let i = 0; i < betLots.length; i++) {
      const betLot = betLots[i];
      const validBets = betLot.numbers.filter(number => number && number.length === 2);
      if (validBets.length > 0) {
        hasValidBet = true;
        if (!betLot.stake) {
          errorIndex = i;
          break;
        }
      }
    }
    const { onShowPopup } = this.props;
    if (errorIndex !== undefined) {
      onShowPopup(
        'Missing Stake',
        `You forgot to set your stake for lot number ${errorIndex +
          1}. Press the stake button at lot number ${errorIndex + 1} to input your stake.`
      );
      return false;
    }
    if (!hasValidBet) {
      onShowPopup(
        'Missing Number',
        'You did not place any valid number. A bet number must have 2 digits. For example: try input 08 not just 8.'
      );
      return false;
    }
    return true;
  }

  handleConfirmedBets() {
    const { onPlaceBets } = this.props;
    if (!onPlaceBets) return;
    //Copy state to post
    const betLots = this.state.betLots.slice(0).map(betLot => {
      betLot.numbers = betLot.numbers.slice(0);
      return { ...betLot };
    });
    const placedBets = betLots
      .map(betLot => {
        betLot.numbers = betLot.numbers
          .filter(number => number && number.length === 2)
          .map(number => parseInt(number, 10));
        if (betLot.numbers.length > 0) {
          return {
            numbers: betLot.numbers,
            stake: betLot.stake,
            special: betLot.special
          };
        }
        return undefined;
      })
      .filter(Boolean);

    const defaultBetLots = [{ numbers: [], stake: 0, special: false, activeIndex: 0, lotTotal: 0 }];
    placedBets.length > 0 &&
      onPlaceBets(placedBets, () => {
        this.setState({
          betLots: defaultBetLots.map(betLot => {
            betLot.numbers = betLot.numbers.slice(0);
            return betLot;
          }),
          totalStake: 0,
          focusLotIndex: 0,
          isStaking: false,
          stakingValue: '',
          stakingMultiply: 0,
          stakingLotIndex: 0,
          error: {
            title: '',
            notice: ''
          },
          hasError: false,
          isConfirming: false
        });
        this.props.navigation.pop();
        chip.play();
      });
  }

  render() {
    const {
      betLots,
      focusLotIndex,
      isStaking,
      stakingValue,
      stakingMultiply,
      isConfirming
    } = this.state;

    const { isLoading, dimensions } = this.props;

    return (
      <DikeBackground>
        <Content ref={node => (this.content = node)}>
          {betLots.map((betLot, i) => (
            <BetLotCard
              key={i}
              isFirst={i === 0}
              value={betLot.numbers}
              stake={betLot.stake}
              isFocus={focusLotIndex === i}
              isSpecial={betLot.special}
              activeIndex={betLot.activeIndex}
              lotTotal={betLot.lotTotal}
              onBetLotPress={() => {
                this.setState({ focusLotIndex: i });
                tap.play();
              }}
              onRemovePress={() => {
                this.handleRemoveBetLot(i);
                tap.play();
              }}
              onStakePress={() => {
                this.handleStakePress(i);
                tap.play();
              }}
            />
          ))}
        </Content>
        <View style={styles.addBetWrapper}>
          <Card transparent style={styles.addBetCard}>
            <CardItem borderBottom>
              <Left>
                <TouchableOpacity
                  onPressIn={() => {
                    this.handleAddBetLot();
                    tap.play();
                  }}
                  style={styles.addBetLine}>
                  <View bgDikeNormal center btnBg>
                    <Text bold>+</Text>
                  </View>
                  <Text highlight> Add a new bet</Text>
                </TouchableOpacity>
              </Left>
              <Right>
                <View horizontal>
                  <H3 highlight>Total: </H3>
                  <H3 bold highlight>
                    {new BigNumber(this.state.totalStake).toFormat(0)}
                  </H3>
                </View>
              </Right>
            </CardItem>
            <CardItem center>
              <View half>
                <Button
                  block
                  onPress={() => {
                    this.handlePlaceBetPress();
                    tap.play();
                  }}>
                  <Text>Place Bet</Text>
                </Button>
              </View>
            </CardItem>
          </Card>
        </View>
        <Keyboard
          height={(dimensions.height - 4) * 0.4}
          onInput={value => (isStaking ? this.handleStakeInput(value) : this.handleBetInput(value))}
          hideModeBtn={isStaking}
          isSpecial={betLots[focusLotIndex].special}
        />
        {isStaking && (
          <StakeDialog
            onDismiss={() => this.setState({ isStaking: false })}
            value={stakingValue}
            multiply={stakingMultiply}
            onSubmit={() => {
              this.handleStakeSubmit();
              chip.play();
            }}
          />
        )}
        {isConfirming && (
          <ConfirmDialog
            onDismiss={() => this.setState({ isConfirming: false })}
            onSubmit={() => {
              this.handleConfirmedBets();
              tap.play();
            }}
            title="Are you sure?"
            notice="You're about to bet on your chosen numbers. Good luck!"
          />
        )}

        {isLoading && <ScreenOverlayLoading />}
        <AutoPopup />
      </DikeBackground>
    );
  }
}

export default withDimensions()(PlaceBetsScreen);
