import React from 'react';
import BigNumber from 'bignumber.js';

import { TouchableOpacity } from 'react-native';
import { View, Text, Button, Icon, Card, CardItem, Right, Left } from 'native-base';
import BetLotInput from './BetLotInput.component';

export default ({
  isFirst,
  isFocus,
  value = [],
  stake = 0,
  isSpecial,
  onBetLotPress,
  onRemovePress,
  onStakePress,
  activeIndex,
  lotTotal
}) => (
  <Card bgDark first={isFirst} smallMargin>
    <CardItem>
      <View borderRight style={{ flex: 0.7 }}>
        <TouchableOpacity onPressIn={() => onBetLotPress()}>
          <BetLotInput
            isFocus={isFocus}
            value={value}
            isSpecial={isSpecial}
            activeIndex={activeIndex}
          />
        </TouchableOpacity>
      </View>
      <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
        <TouchableOpacity onPressIn={() => onRemovePress()}>
          <View bgDikeSpecial center btnBg>
            <Icon name="ios-close" />
          </View>
        </TouchableOpacity>
        <Button block colMargin onPress={() => onStakePress()} small>
          <Text uppercase={false}>Stake</Text>
        </Button>
        <Text>x {new BigNumber(stake).toFormat(0)}</Text>
      </View>
    </CardItem>
    <CardItem endFlexBody>
      <Left />
      <Right>
        <View horizontal revert>
          <Text>Lot total: </Text>
          <Text bold>{new BigNumber(lotTotal).toFormat(0)}</Text>
        </View>
      </Right>
    </CardItem>
  </Card>
);
