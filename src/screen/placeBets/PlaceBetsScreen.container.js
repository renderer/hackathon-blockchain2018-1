import { connect } from 'react-redux';
import PlaceBetsScreen from './PlaceBetsScreen.component';

import { placeBets } from 'dike/store/drawing/drawing.actions';
import { setPlaceBetsPopupProps } from 'dike/store/app/app.actions';

export default connect(
  ({ app }) => ({
    isLoading: app.isPlaceBetsScreenLoading
  }),
  dispatch => ({
    onPlaceBets(bets, onSuccess) {
      dispatch(placeBets(bets, onSuccess));
    },
    onShowPopup(title, notice) {
      dispatch(
        setPlaceBetsPopupProps({
          isVisible: true,
          title,
          notice
        })
      );
    }
  })
)(PlaceBetsScreen);
