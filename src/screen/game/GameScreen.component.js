import { tap } from 'dike/utils/sounds';
import React from 'react';
import { Content } from 'native-base';

import DikeBackground from 'dike/components/DikeBackground';

import DrawingEnd from './DrawingEnd.component';
import CurrentDrawingCard from './CurrentDrawingCard.component';
import PreviousDrawingCard from './PreviousDrawingCard.container';

export default ({ drawing, navigation }) => (
  <DikeBackground>
    <Content>
      {!drawing && <DrawingEnd />}
      {drawing && (
        <CurrentDrawingCard
          drawing={drawing}
          onNavigate={() => {
            navigation.navigate('PlaceBets');
            tap.play();
          }}
        />
      )}
      <PreviousDrawingCard
        onNavigate={drawing => {
          navigation.navigate('DrawingDetail', { drawingId: drawing.id });
          tap.play();
        }}
      />
    </Content>
  </DikeBackground>
);
