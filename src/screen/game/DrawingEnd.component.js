import React from 'react';
import { View, Text, Button, H2 } from 'native-base';

export default () => (
  <View marginAround>
    <H2 bold viewTitle>
      Drawing end
    </H2>
    <Text>Please wait for the next drawing.</Text>
    <Text endDescription>Check out the previous drawing's result. Good luck!</Text>
    <View fullWidth center>
      <View half>
        <Button large block disabled>
          <Text primary>Place bets</Text>
        </Button>
      </View>
    </View>
  </View>
);
