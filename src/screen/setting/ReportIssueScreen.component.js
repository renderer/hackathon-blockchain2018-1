import theme from 'dike/theme';
import { tap } from 'dike/utils/sounds';
import React, { Component } from 'react';

import { View, Form, Item, Input, Text, Button, Content, Textarea, H1 } from 'native-base';
import DikeBackground from 'dike/components/DikeBackground';
import ScreenOverlayLoading from 'dike/components/ScreenOverlayLoading';

export default class ReportIssueScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      error: {},
      isSuccess: false
    };
  }

  handleSubmit() {
    const { title, description } = this.state;
    const isValidTitle = title && title.length > 10;
    const isValidDescription = description && description.length > 20;
    if (!isValidTitle || !isValidDescription) {
      this.setState({ error: { title: !isValidTitle, description: !isValidDescription } });
      return;
    }
    this.props.onReportIssue({ title, description }, () => {
      this.setState({ isSuccess: true });
    });
  }

  render() {
    const { title, description, error, isSuccess } = this.state;
    const { navigation, isLoading } = this.props;

    return (
      <DikeBackground>
        {!isSuccess && (
          <Content>
            <View full center>
              <View style={{ width: '80%' }} center>
                <Form full>
                  <Item noLabel error={error.title}>
                    <Input
                      placeholder="Title"
                      value={title}
                      onChangeText={v => {
                        const nextState = { title: v };
                        if (error.title) {
                          error.title = v && v.length <= 10;
                          nextState.error = error;
                        }
                        this.setState(nextState);
                      }}
                    />
                  </Item>
                  {!!error.title && (
                    <Text small error>
                      *We need a more meaningful title
                    </Text>
                  )}

                  <Item noLabel style={{ borderBottomWidth: 0 }}>
                    <Textarea
                      rowSpan={10}
                      placeholder="Description"
                      value={description}
                      placeholderTextColor={theme.text.placeholder}
                      style={{
                        width: '100%',
                        borderWidth: 1,
                        borderColor: error.description
                          ? theme.inputErrorBorderColor
                          : theme.inputBorderColor,
                        fontSize: theme.inputFontSize
                      }}
                      onChangeText={v => {
                        const nextState = { description: v };
                        if (error.description) {
                          error.description = v && v.length <= 20;
                          nextState.error = error;
                        }
                        this.setState(nextState);
                      }}
                    />
                  </Item>
                  {!!error.description && (
                    <Text small error>
                      *Please provide more detailed description
                    </Text>
                  )}
                </Form>

                <View half fullFormFooter>
                  <Button
                    block
                    primary
                    onPress={() => {
                      this.handleSubmit();
                      tap.play();
                    }}>
                    <Text>Send</Text>
                  </Button>
                  <Button
                    block
                    info
                    onPress={() => {
                      navigation.goBack();
                      tap.play();
                    }}>
                    <Text>Cancel</Text>
                  </Button>
                </View>
              </View>
            </View>
          </Content>
        )}
        {isSuccess && (
          <View full center>
            <View style={{ width: '80%' }} center>
              <H1 bold>Thank you!</H1>
              <Text endDescription>
                Your report has been sent to support team. A supporter will contact you shortly if
                needed!
              </Text>
              <View fullWidth center>
                <View half fullFormFooter>
                  <Button
                    info
                    block
                    onPress={() => {
                      navigation.goBack();
                      tap.play();
                    }}>
                    <Text>Back</Text>
                  </Button>
                </View>
              </View>
            </View>
          </View>
        )}
        {isLoading && <ScreenOverlayLoading />}
      </DikeBackground>
    );
  }
}
