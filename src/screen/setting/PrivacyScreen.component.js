import React from 'react';
import {
  Text,
  Switch,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Icon,
  Button
} from 'native-base';
import DikeBackground from 'dike/components/DikeBackground';

export default class PrivacyScreeen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allowMember: props.allowMember,
      allowService: props.allowService
    };
  }

  handleSubmitPrivacy() {
    const { updatePrivacyRule } = this.props;
    const { allowMember, allowService } = this.state;
    updatePrivacyRule(allowMember, allowService);
  }

  render() {
    const { allowMember, allowService } = this.state;

    return (
      <DikeBackground>
        <Content>
          <ListItem icon>
            <Left>
              <Icon active name="ios-person" />
            </Left>
            <Body>
              <Text>Allow member detect</Text>
            </Body>
            <Right>
              <Switch
                value={allowMember}
                onValueChange={vl => this.setState({ allowMember: vl })}
              />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Icon active name="ios-apps" />
            </Left>
            <Body>
              <Text>Allow service detect</Text>
            </Body>
            <Right>
              <Switch
                value={allowService}
                onValueChange={vl => this.setState({ allowService: vl })}
              />
            </Right>
          </ListItem>
          <Button
            block
            primary
            colMargin
            style={{ alignSelf: 'center' }}
            onPress={() => this.handleSubmitPrivacy()}>
            <Text style={{ margin: 5 }}>Submit</Text>
          </Button>
        </Content>
      </DikeBackground>
    );
  }
}
