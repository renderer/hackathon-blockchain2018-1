import { connect } from 'react-redux';
import ReportIssueScreen from './ReportIssueScreen.component';

import { reportIssue } from 'dike/store/user/user.actions';

export default connect(
  ({ app }) => ({
    isLoading: app.isReportIssueScreenLoading
  }),
  dispatch => ({
    onReportIssue({ title, description }, onSuccess) {
      dispatch(reportIssue({ title, description }, onSuccess));
    }
  })
)(ReportIssueScreen);
