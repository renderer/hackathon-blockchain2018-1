import SettingScreen from './SettingScreen.container';
import ReportIssueScreen from './ReportIssueScreen.container';
import ChangePasswordScreen from './ChangePasswordScreen.container';

export { ChangePasswordScreen, SettingScreen, ReportIssueScreen };
