import React from 'react';

import { Card, CardItem, Left, Right, View, Text } from 'native-base';

import DikeResult from 'dike/components/DikeResult';
import DrawingShareButton from 'dike/components/DrawingShareButton';
import TimeRow from 'dike/components/TimeRow';

export default ({ drawing }) => (
  <Card transparent first>
    <CardItem header borderBottom>
      <Left>
        <TimeRow time={drawing.endTime} />
      </Left>
      <Right>
        <Text>#{drawing.serial}</Text>
      </Right>
    </CardItem>
    <CardItem>
      <DikeResult result={drawing.result} />
    </CardItem>
    <CardItem endFlexBody footer center>
      <View half>
        <DrawingShareButton drawing={drawing} />
      </View>
    </CardItem>
  </Card>
);
