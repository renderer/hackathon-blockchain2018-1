import React from 'react';
import Leaderboard from 'dike/components/Leaderboard';

import { Card, CardItem } from 'native-base';

export default ({ leaderboard }) => (
  <Card bgSemiDark noHeader>
    <CardItem fullWidth>
      <Leaderboard
        type="reward"
        leaderboard={leaderboard.map(row => ({ username: row.username, points: row.reward }))}
      />
    </CardItem>
  </Card>
);
