import { connect } from 'react-redux';
import PeopleScreen from './PeopleScreen.component';

import { fetchUserData, sendPoints } from 'dike/store/user/user.actions';

export default connect(
  ({ app, user }) => ({
    isLoading: app.isPeopleScreenLoading,
    privateKey: user.ethPrivateKey
  }),
  dispatch => ({
    fetchUserData(userId, cb) {
      dispatch(fetchUserData(userId, data => cb(data)));
    },
    onSendPoints(address, amount, privateKey) {
      dispatch(sendPoints(address, amount, privateKey));
    }
  })
)(PeopleScreen);
