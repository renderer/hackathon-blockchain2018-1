import { connect } from 'react-redux';
import PeopleListScreen from './PeopleListScreen.component';

import { fetchAllNearby } from 'dike/store/user/user.actions';
import { startAdvertising, stopAdvertising } from 'dike/store/nearby/nearby.actions';

export default connect(
  ({ nearby }) => ({
    nearbyList: nearby.nearbyUsers,
    advertising: nearby.advertising
  }),
  dispatch => ({
    fetchAllNearby(userId, cb) {
      dispatch(fetchAllNearby(userId, data => cb(data)));
    },
    startAdvertising() {
      dispatch(startAdvertising());
    },
    stopAdvertising() {
      dispatch(stopAdvertising());
    }
  })
)(PeopleListScreen);
