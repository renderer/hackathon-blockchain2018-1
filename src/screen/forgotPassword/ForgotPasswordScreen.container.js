import { connect } from 'react-redux';
import ForgotPasswordScreen from './ForgotPasswordScreen.component';

import { sendForgotPasswordEmail } from 'dike/store/user/user.actions';

export default connect(
  ({ app }) => ({
    isLoading: app.forgotPasswordScreen.isLoading,
    submitError: app.forgotPasswordScreen.error
  }),
  dispatch => ({
    onSubmit(email, onSuccess, options) {
      dispatch(sendForgotPasswordEmail(email, onSuccess, options));
    }
  })
)(ForgotPasswordScreen);
