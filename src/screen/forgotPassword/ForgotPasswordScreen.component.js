import { tap } from 'dike/utils/sounds';
import React, { Component } from 'react';

import { Text, Input, Button, Item, View, H1, Form } from 'native-base';
import DikeBackground from 'dike/components/DikeBackground';
import TextButton from 'dike/components/TextButton';
import ScreenOverlayLoading from 'dike/components/ScreenOverlayLoading';

import { validateEmail } from 'dike/utils/common/validators';
import { setStorageItem, getStorageItem } from 'dike/utils/asyncStorage';

export default class ForgotPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      error: {},
      sentMailSuccess: false,
      haveEmailStored: false
    };
    this.getStoredEmail();
  }

  async getStoredEmail() {
    const userEmail = await getStorageItem('userEmail');
    if (userEmail) {
      this.setState({ haveEmailStored: true });
    }
  }

  handleSubmit() {
    const { email } = this.state;
    const isValidEmail = validateEmail(email);
    if (!isValidEmail) {
      this.setState({
        error: { email: !isValidEmail }
      });
      return;
    }
    this.props.onSubmit(
      email,
      () => {
        setStorageItem('userEmail', email);
        this.setState({ sentMailSuccess: true });
      },
      { noToast: true }
    );
  }

  render() {
    const { email, error, sentMailSuccess, haveEmailStored } = this.state;
    const { navigation, isLoading, submitError } = this.props;

    return (
      <DikeBackground>
        <View full center>
          {!sentMailSuccess && (
            <View authScreenWrapper>
              <H1 bold>What's my password?</H1>
              <Text endDescription>If you have forgotten your password you can reset it here</Text>
              {!!submitError && (
                <Text error small>
                  *{submitError}
                </Text>
              )}
              <Form full>
                <Item noLabel error={error.email}>
                  <Input
                    keyboardType="email-address"
                    autoCapitalize="none"
                    value={email}
                    onChangeText={v => {
                      const nextState = { email: v };
                      if (error.email) {
                        error.email = !validateEmail(v);
                        nextState.error = error;
                      }
                      this.setState(nextState);
                    }}
                    placeholder="Email address"
                  />
                </Item>
                {!!error.email && (
                  <Text error small>
                    *Invalid email
                  </Text>
                )}
              </Form>
              <View fullWidth center>
                <View half fullFormFooter>
                  <Button block primary onPress={() => this.handleSubmit()}>
                    <Text>Reset</Text>
                  </Button>
                  <Button
                    info
                    block
                    onPress={() => {
                      navigation.goBack();
                      tap.play();
                    }}>
                    <Text>Cancel</Text>
                  </Button>
                  {haveEmailStored && (
                    <TextButton
                      TextProps={{ small: true, italic: true }}
                      text="Already received email?"
                      onPress={() => {
                        navigation.navigate('SetPassword');
                        tap.play();
                      }}
                    />
                  )}
                </View>
              </View>
            </View>
          )}
          {sentMailSuccess && (
            <View authScreenWrapper>
              <H1 bold>We sent an email</H1>
              <Text endDescription>
                Tap the next button if you have received an email from our support team.
              </Text>
              <View fullWidth center>
                <View half fullFormFooter>
                  <Button block primary onPress={() => navigation.navigate('SetPassword')}>
                    <Text>Next</Text>
                  </Button>
                </View>
              </View>
            </View>
          )}
        </View>
        {isLoading && <ScreenOverlayLoading />}
      </DikeBackground>
    );
  }
}
