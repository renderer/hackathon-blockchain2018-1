import React from 'react';

import './global';

import { AppRegistry, Platform } from 'react-native';
import { App } from './src/app';

const prefix = Platform.OS == 'android' ? 'dikeapp://dikeapp/' : 'dikeapp://';
const MainApp = () => <App uriPrefix={prefix} />;

AppRegistry.registerComponent('dikeapp', () => MainApp);
